#pragma once
/// \file AABB.h
/// \brief Contains Smits Axis Aligned Bounding Box implementation.

#include "Common.h"
#include "Vector3.h"
#include "Ray.h"
#include "Packet.h"

///
///
///
struct AABB {
//TODO breaks building, if we switch to private
//private:
    Vector3 bounds[2];

public:
    //TODO convert
    //RTSPH_FLOAT nx, ny, nz;
    //RTSPH_FLOAT xx, xy, xz;
    
    ///
    ///
    ///
    AABB() {
        bounds[0] = Vector3( real_inf);
        bounds[1] = Vector3(-real_inf);

        //nx = ny = nz =  real_inf;
        //xx = xy = xz = -real_inf;
    }
    
    ///
    ///
    ///
    void expand(const Vector3& pnt) {
        bounds[0].min(pnt);
        bounds[1].max(pnt);

        //nx = MIN(nx, pnt.x(); ny = MIN(ny, pnt.y(); nz = MIN(nz, pnt.z());
        //xx = MAX(xx, pnt.x(); xy = MAX(xy, pnt.y(); xz = MAX(xz, pnt.z());
    }
    
    ///
    ///
    ///
    void expand(const AABB& aabb) {
        //max should only be able to increase max
        //min should only be able to increase min
        bounds[0].min(aabb.bounds[0]);
        bounds[1].max(aabb.bounds[1]);

        //nx = MIN(nx, aabb.nx); ny = MIN(ny, aabb.ny); nz = MIN(nz, aabb.nz);
        //xx = MAX(xx, aabb.xx); xy = MAX(xy, aabb.xy); xz = MAX(xz, aabb.xz);
    }

    /// \brief Calculates the surface area for Surface Area Heuristic of the AABB.
    ///
    ///
    RTSPH_FLOAT surfacearea() const {
        Vector3 dv;
        dv += bounds[1];
        dv -= bounds[0];

        //RTSPH_FLOAT dx = xx - nx, dy = xy - ny, dz = xz - nz;
        //RTSPH_FLOAT half = dx*dy + dx*dz + dy*dz;

        RTSPH_FLOAT sa = (dv[0] * dv[1]) 
                  + (dv[0] * dv[2]) 
                  + (dv[1] * dv[2]);

        if(sa < 0.0) 
            return 0.0;
        else
            return 2.0 * sa;
    }

    /// \brief Resets the AABB to the original.
    ///
    ///
    void clear() {
        bounds[0].set( real_inf);
        bounds[1].set(-real_inf);

        //nx = ny = nz =  real_inf;
        //xx = xy = xz = -real_inf;
    };

    /// \brief Smit Ray-AABB intersection algorithm.
    ///
    ///
    inline bool intersect(const Ray& ray) const;

    #if defined __SIMD__
    ///
    ///
    ///
    inline bool intersect(const Packet& packet) const;
    #endif
};

bool AABB::intersect(const Ray& ray) const {
    RTSPH_FLOAT t0, t1;
    RTSPH_FLOAT tymin, tymax, tzmin, tzmax;

    //todo replace
    //RTSPH_FLOAT divx = 1.0 / ray.direction.x();
    RTSPH_FLOAT divx = ray.invdir.x();
    if(divx >= 0) {
        //t0 = (bounds[0].x() - ray.origin.x()) * divx;
        t0 = (bounds[0].x()*divx) - (ray.origin.x()*divx);
        //t1 = (bounds[1].x() - ray.origin.x()) * divx;
        t1 = (bounds[1].x()*divx) - (ray.origin.x()*divx);
    }
    else {
        t0 = (bounds[1].x() - ray.origin.x()) * divx;
        t1 = (bounds[0].x() - ray.origin.x()) * divx;
    }

    //t0 = (bounds[ray.xmin].x()*divx) - (ray.origin.x()*divx);
    //t1 = (bounds[ray.xmax].x()*divx) - (ray.origin.x()*divx);

    //todo replace
    //RTSPH_FLOAT divy = 1.0 / ray.direction.y();
    RTSPH_FLOAT divy = ray.invdir.y();
    if(divy >= 0) {
        tymin = (bounds[0].y() - ray.origin.y()) * divy;
        tymax = (bounds[1].y() - ray.origin.y()) * divy;
    }
    else {
        tymin = (bounds[1].y() - ray.origin.y()) * divy;
        tymax = (bounds[0].y() - ray.origin.y()) * divy;
    }

    //early exit 1
    if((t0 > tymax) || (tymin > t1))
        return false;

    t0 = MAX(t0, tymin);
    t1 = MIN(t1, tymax);

    //todo replace
    //RTSPH_FLOAT divz = 1.0 / ray.direction.z();
    RTSPH_FLOAT divz = ray.invdir.z();
    if(divz >= 0) {
        tzmin = (bounds[0].z() - ray.origin.z()) * divz;
        tzmax = (bounds[1].z() - ray.origin.z()) * divz;
    }
    else {
        tzmin = (bounds[1].z() - ray.origin.z()) * divz;
        tzmax = (bounds[0].z() - ray.origin.z()) * divz;
    }
    
    //early exit 2
    if((t0 > tzmax) || (tzmin > t1))
        return false;

    t0 = MAX(t0, tzmin);
    t1 = MIN(t1, tzmax);

    //ray limits
    return (t0 <= ray.tmax) && (0.0 <= t1);
}

#if defined __SIMD__
bool AABB::intersect(const Packet& packet) const {
    //axis-x slab
    RTSPH_FLOATw bminx = set1pd(bounds[0].x());
    RTSPH_FLOATw bmaxx = set1pd(bounds[1].x());
    RTSPH_FLOATw bminy = set1pd(bounds[0].y());
    RTSPH_FLOATw bmaxy = set1pd(bounds[1].y());
    RTSPH_FLOATw bminz = set1pd(bounds[0].z());
    RTSPH_FLOATw bmaxz = set1pd(bounds[1].z());
    RTSPH_FLOATw any = falsew;

    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        //RTSPH_FLOATw l1x = divpd(subpd(bminx, packet.ox[i]), packet.dx[i]);
        //RTSPH_FLOATw l2x = divpd(subpd(bmaxx, packet.ox[i]), packet.dx[i]);
        RTSPH_FLOATw l1x = mulpd(subpd(bminx, packet.ox[i]), packet.invdx[i]);
        RTSPH_FLOATw l2x = mulpd(subpd(bmaxx, packet.ox[i]), packet.invdx[i]);

        RTSPH_FLOATw lmin = minpd(l1x, l2x);
        RTSPH_FLOATw lmax = maxpd(l1x, l2x);

        //RTSPH_FLOATw l1y = divpd(subpd(bminy, packet.oy[i]), packet.dy[i]);
        //RTSPH_FLOATw l2y = divpd(subpd(bmaxy, packet.oy[i]), packet.dy[i]);
        RTSPH_FLOATw l1y = mulpd(subpd(bminy, packet.oy[i]), packet.invdy[i]);
        RTSPH_FLOATw l2y = mulpd(subpd(bmaxy, packet.oy[i]), packet.invdy[i]);

        lmin = maxpd(minpd(l1y, l2y), lmin);
        lmax = minpd(maxpd(l1y, l2y), lmax);
    
        //RTSPH_FLOATw l1z = divpd(subpd(bminz, packet.oz[i]), packet.dz[i]);
        //RTSPH_FLOATw l2z = divpd(subpd(bmaxz, packet.oz[i]), packet.dz[i]);
        RTSPH_FLOATw l1z = mulpd(subpd(bminz, packet.oz[i]), packet.invdz[i]);
        RTSPH_FLOATw l2z = mulpd(subpd(bmaxz, packet.oz[i]), packet.invdz[i]);

        lmin = maxpd(minpd(l1z, l2z), lmin);
        lmax = minpd(maxpd(l1z, l2z), lmax);   

        //any intersections
        RTSPH_FLOATw omask = cmpltpd(lmin, lmax);

        //ray limit mask
        RTSPH_FLOATw xmask = cmplepd(lmin, packet.tmax[i]);
        //RTSPH_FLOATw nmask = cmplepd(packet.tmin[i], lmax);
        RTSPH_FLOATw nmask = cmplepd(zero, lmax);
        
        RTSPH_FLOATw fmask = andpd(omask, andpd(xmask, nmask));
    
        //any filtered
        any = orpd(any, fmask);
    }
    return movemaskpd(any) > 0;
}
#endif
