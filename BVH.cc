#include "BVH.h"
#include "Weight.h"

RTSPH_FLOAT BVH::weightRay(const Ray& ray, const RTSPH_FLOAT* qs) const {
    RTSPH_FLOAT rayweight = 0.0;

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(ray)) {
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
            else {
                rayweight += weightNode(ray, inode, qs);
            }
        }
    }

    return rayweight;
}

RTSPH_FLOAT BVH::weightRay(const Ray& ray) const {
    RTSPH_FLOAT rayweight = 0.0;

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(ray)) {
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
            else {
                rayweight += weightNode(ray, inode);
            }
        }
    }

    return rayweight;
}

RTSPH_FLOAT BVH::weightNode(const Ray& ray, uint32_t inode, const RTSPH_FLOAT* qs) const {
    RTSPH_FLOAT nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        nodeweight += kernelnb2(nb2)*spheres.invh2[isph]*qs[isph];
    }

    return nodeweight;
}

/*------RAW------*/
/*RTSPH_FLOAT BVH::weightNode(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        nodeweight += kernelnb2(nb2) * spheres.invh2[isph];
    }

    return nodeweight;
}*/

/*------UNROLL4------*/
RTSPH_FLOAT BVH::weightNode(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight0 = 0.0;
    RTSPH_FLOAT nodeweight1 = 0.0;
    RTSPH_FLOAT nodeweight2 = 0.0;
    RTSPH_FLOAT nodeweight3 = 0.0;

    const BVHNode& node = nodes[inode];

    //calculate sphere unrolled 4
    uint32_t inc = 0;
    if(4 < node.run) {
        for(; inc < node.run-3; inc += 4) {
            uint32_t isph0 = node.offset + inc + 0;
            uint32_t isph1 = node.offset + inc + 1;
            uint32_t isph2 = node.offset + inc + 2;
            uint32_t isph3 = node.offset + inc + 3;

            //intersect
            RTSPH_FLOAT nb20 = spheres.intersect(isph0, ray);
            RTSPH_FLOAT nb21 = spheres.intersect(isph1, ray);
            RTSPH_FLOAT nb22 = spheres.intersect(isph2, ray);
            RTSPH_FLOAT nb23 = spheres.intersect(isph3, ray);

            //weight
            RTSPH_FLOAT w2D0 = kernelnb2(nb20) * spheres.invh2[isph0];
            RTSPH_FLOAT w2D1 = kernelnb2(nb21) * spheres.invh2[isph1];
            RTSPH_FLOAT w2D2 = kernelnb2(nb22) * spheres.invh2[isph2];
            RTSPH_FLOAT w2D3 = kernelnb2(nb23) * spheres.invh2[isph3];

            //contribute
            nodeweight0 += w2D0;
            nodeweight1 += w2D1;
            nodeweight2 += w2D2;
            nodeweight3 += w2D3;
        }
    }

    //calculate rest without unrolling
    for(; inc < node.run; inc++) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        RTSPH_FLOAT k2D = kernelnb2(nb2);
        RTSPH_FLOAT w2D = k2D * spheres.invh2[isph];

        //contribute
        nodeweight0 += w2D;
    }

    return (nodeweight0+nodeweight1) + (nodeweight2+nodeweight3);
}

/*RTSPH_FLOAT BVH::weightOfNodeQ(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        RTSPH_FLOAT k2D = kernelnb2(nb2);
        //todo seperate q from spheres, avoid copying
        RTSPH_FLOAT w2D = k2D * spheres.invh2[isph] * spheres.q[isph];

        //contribute
        nodeweight += w2D;
    }

    return nodeweight;
}*/

//**************RAW**************
/*RTSPH_FLOAT BVH::weightOfNodeU(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        nodeweight += kernelnb2(nb2) * spheres.invh2[isph];
    }

    return nodeweight;
}*/

//***************PIPELINING***************
//XXX not working? order in correct?
/*RTSPH_FLOAT BVH::weightOfNodeU(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight = 0.0;
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    RTSPH_FLOAT nb2 = spheres.intersect(0, ray);

    uint32_t inc = 1;
    for(; inc < node.run; inc++) {
        uint32_t isph = node.offset + inc;
 
        //intersecting current
        RTSPH_FLOAT tnb2 = spheres.intersect(isph, ray);       

        //weighting previous
        nodeweight += kernelnb2(nb2) * spheres.invh2[isph-1];

        //copy
        nb2 = tnb2;
    }

    //weight last
    nodeweight += kernelnb2(nb2) * spheres.invh2[node.offset+inc];

    return nodeweight;
}*/

//***************UNROLL***************
/*RTSPH_FLOAT BVH::weightOfNodeU(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight0 = 0.0;
    RTSPH_FLOAT nodeweight1 = 0.0;

    const BVHNode& node = nodes[inode];

    //calculate sphere unrolled 2
    uint32_t inc = 0;
    if(node.run > 2) {
        for(; inc < node.run-1; inc += 2) {
            uint32_t isph0 = node.offset + inc + 0;
            uint32_t isph1 = node.offset + inc + 1;

            //intersect
            RTSPH_FLOAT nb20 = spheres.intersect(isph0, ray);
            RTSPH_FLOAT nb21 = spheres.intersect(isph1, ray);

            //weight
            RTSPH_FLOAT w2D0 = kernelnb2(nb20) * spheres.invh2[isph0];
            RTSPH_FLOAT w2D1 = kernelnb2(nb21) * spheres.invh2[isph1];

            //contribute
            nodeweight0 += w2D0;
            nodeweight1 += w2D1;
        }
    }

    //calculate rest without unrolling
    for(; inc < node.run; inc++) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        RTSPH_FLOAT k2D = kernelnb2(nb2);
        RTSPH_FLOAT w2D = k2D * spheres.invh2[isph];

        //contribute
        nodeweight0 += w2D;
    }

    return (nodeweight0+nodeweight1);
}*/

//***************UNROLL***************
/*RTSPH_FLOAT BVH::weightOfNodeU(const Ray& ray, uint32_t inode) const {
    RTSPH_FLOAT nodeweight0 = 0.0;
    RTSPH_FLOAT nodeweight1 = 0.0;
    RTSPH_FLOAT nodeweight2 = 0.0;
    RTSPH_FLOAT nodeweight3 = 0.0;

    const BVHNode& node = nodes[inode];

    //calculate sphere unrolled 4
    uint32_t inc = 0;
    if(node.run > 4) {
        for(; inc < node.run-3; inc += 4) {
            uint32_t isph0 = node.offset + inc + 0;
            uint32_t isph1 = node.offset + inc + 1;
            uint32_t isph2 = node.offset + inc + 2;
            uint32_t isph3 = node.offset + inc + 3;

            //intersect
            RTSPH_FLOAT nb20 = spheres.intersect(isph0, ray);
            RTSPH_FLOAT nb21 = spheres.intersect(isph1, ray);
            RTSPH_FLOAT nb22 = spheres.intersect(isph2, ray);
            RTSPH_FLOAT nb23 = spheres.intersect(isph3, ray);

            //weight
            RTSPH_FLOAT w2D0 = kernelnb2(nb20) * spheres.invh2[isph0];
            RTSPH_FLOAT w2D1 = kernelnb2(nb21) * spheres.invh2[isph1];
            RTSPH_FLOAT w2D2 = kernelnb2(nb22) * spheres.invh2[isph2];
            RTSPH_FLOAT w2D3 = kernelnb2(nb23) * spheres.invh2[isph3];

            //contribute
            nodeweight0 += w2D0;
            nodeweight1 += w2D1;
            nodeweight2 += w2D2;
            nodeweight3 += w2D3;
        }
    }

    //calculate rest without unrolling
    for(; inc < node.run; inc++) {
        uint32_t isph = node.offset + inc;
        
        //intersect
        RTSPH_FLOAT nb2 = spheres.intersect(isph, ray);

        //weight
        RTSPH_FLOAT k2D = kernelnb2(nb2);
        RTSPH_FLOAT w2D = k2D * spheres.invh2[isph];

        //contribute
        nodeweight0 += w2D;
    }

    return (nodeweight0+nodeweight1) + (nodeweight2+nodeweight3);
}*/

/*RTSPH_FLOAT BVH::columnDensityQ(const Ray& ray) const {
    RTSPH_FLOAT rayweight = 0.0;

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(ray)) {
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
            else {
                rayweight += weightOfNodeQ(ray, inode);
            }
        }
    }

    return rayweight;
}

RTSPH_FLOAT BVH::columnDensityU(const Ray& ray) const {
    RTSPH_FLOAT rayweight = 0.0;

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(ray)) {
            //process internal node
            if(!node.isLeaf()) {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
            //process leaf node
            else {
                rayweight += weightOfNodeU(ray, inode);
            }
        }
    }

    return rayweight;
}*/

/*#if defined __SIMD__
void BVH::weightOfNode(const Packet& packet, uint32_t inode, RTSPH_FLOATw densities[PACKET_SIZE]) const {
    const BVHNode& node = nodes[inode];

    //for every sphere in node
    for(uint32_t inc = 0; inc < node.run; ++inc) {
        uint32_t isph = node.offset + inc;
        
        //intersect sphere
        PacketIntersect intrst;
        spheres.intersect(isph, packet, intrst);

        //exit if all rays are dead
        if(intrst.isDead()) continue;

        //exit if all rays are dead
        if(intrst.isDead()) continue;

        //set nb2 appropriately to 1.0 if inactive/filtered
        for(size_t i = 0; i < PACKET_SIZE; ++i) {
            RTSPH_FLOATw invflt = andnotpd(intrst.active[i], one);
            RTSPH_FLOATw flted = andpd(intrst.active[i], intrst.nb2[i]);
            intrst.nb2[i] = orpd(flted, invflt);
        }

        //weight intersect
        RTSPH_FLOATw k2D[PACKET_SIZE];
        kernel(intrst.nb2, k2D);

        RTSPH_FLOATw invh2 = set1pd(1.0/(spheres.h2(isph)));
        //XXX q varies with spheres!
        RTSPH_FLOATw qw = set1pd(spheres.q[isph]);
        for(size_t i = 0; i < PACKET_SIZE; ++i) {
            RTSPH_FLOATw weight = mulpd(k2D[i], invh2);
            //weight /= mulpd(weight, qw);
    
            densities[i] = addpd(densities[i], mulpd(weight,qw));
        }
    }
}

void BVH::columnDensity(const Packet& packet, RTSPH_FLOATw densities[PACKET_SIZE]) const {
    //initialize densities
    for(size_t i = 0; i < PACKET_SIZE; ++i) {
        densities[i] = zero;
    }

    //stack for tree traversal
    std::stack<uint32_t> stack;
    //root
    stack.push(0);

    //exhaustive traversal of tree
    while(!stack.empty()) {
        uint32_t inode = stack.top(); stack.pop();
        const BVHNode& node = nodes[inode];

        //intersect node
        if(node.bbox.intersect(packet)) {
            if(node.isLeaf()) {
                weightOfNode(packet, inode, densities);
            }
            //process internal node
            else {
                //push right node
                stack.push(node.right);
                //push left node, always to the right
                stack.push(inode+1);
            }
        }
    }
}

#endif*/
