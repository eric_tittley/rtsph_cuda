#pragma once

#include <stack>
#include <vector>

#include "Common.h"
#include "BVHNode.h"
#include "Spheres.h"

struct BVH {
	//access required for building
	Spheres spheres;
	std::vector<uint32_t> map;
	std::vector<BVHNode> nodes;

    RTSPH_FLOAT weightRay(const Ray&, const RTSPH_FLOAT*) const;
    RTSPH_FLOAT weightRay(const Ray&) const;

    RTSPH_FLOAT weightNode(const Ray&, uint32_t, const RTSPH_FLOAT*) const;
    RTSPH_FLOAT weightNode(const Ray&, uint32_t) const;
};

/// \brief Prototype of SAH BVH build factory.
///
/// This function builds a SAH BVH out of a collection of spheres.
void mkSAHBVH(const Spheres&, BVH&);
