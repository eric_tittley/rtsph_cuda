#pragma once
/// \file BVHNode.h
/// \brief BVHNode implementation 

#include "AABB.h"

/// \brief 
///
///
struct BVHNode {
    AABB bbox;
    //internal
    uint32_t right;
    //leaf
    uint32_t offset, run;

    inline bool isLeaf() const {
        return (0 != run);
    }

    inline void setInternal() {
        offset = 0; run = 0;
    }

    inline void setIndex(uint32_t m_right) {
        right = m_right;
    }
};
