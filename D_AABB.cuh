#pragma once
/// \file AABB.h
/// \brief Contains Smits Axis Aligned Bounding Box implementation.

#include <cudasupport.h>

#ifdef __CUDACC__
#include "D_Ray.cuh"
#include "float3_util.cuh"
#include "../libManitou_CUDA/Vec3.h"
#endif

struct D_AABB {
  float3 min, max;
  public:
  static ERT_DEVICE D_AABB from_sphere(float3 centre, float radius);
  static ERT_DEVICE D_AABB bb_union(const D_AABB & lhs, const D_AABB & rhs);
  static ERT_DEVICE D_AABB sphere_union(const D_AABB & bb, float3 origin, float radius);
  ERT_DEVICE bool intersect(const D_Ray& rayptr, float3 OoD) const;
};


#ifdef __CUDACC__
inline __device__ D_AABB D_AABB::from_sphere(float3 centre, float radius) {
  D_AABB bb;
  // Always round outwards from the centre
  bb.min.x = __fsub_rd(centre.x, radius);
  bb.min.y = __fsub_rd(centre.y, radius);
  bb.min.z = __fsub_rd(centre.z, radius);

  bb.max.x = __fadd_ru(centre.x, radius);
  bb.max.y = __fadd_ru(centre.y, radius);
  bb.max.z = __fadd_ru(centre.z, radius);
  return bb;
}

inline __device__ D_AABB D_AABB::bb_union(const D_AABB & lhs, const D_AABB & rhs) {
  D_AABB bb;
  bb.min = make_float3(
    std::min(lhs.min.x, rhs.min.x),
    std::min(lhs.min.y, rhs.min.y),
    std::min(lhs.min.z, rhs.min.z));
  bb.max = make_float3(
    std::max(lhs.max.x, rhs.max.x),
    std::max(lhs.max.y, rhs.max.y),
    std::max(lhs.max.z, rhs.max.z));
  return bb;
}

inline __device__ D_AABB D_AABB::sphere_union(const D_AABB & bb, float3 origin, float radius) {
  return bb_union(bb, from_sphere(origin, radius));
}

inline __device__ bool D_AABB::intersect(const D_Ray& ray, float3 OoD) const {
  float x0 = min.x*ray.invdir.x - OoD.x;
  float y0 = min.y*ray.invdir.y - OoD.y;
  float z0 = min.z*ray.invdir.z - OoD.z;
  float x1 = max.x*ray.invdir.x - OoD.x;
  float y1 = max.y*ray.invdir.y - OoD.y;
  float z1 = max.z*ray.invdir.z - OoD.z;

  float tminbox = max4(0, min2(x0,x1), min2(y0,y1), min2(z0,z1));
  float tmaxbox = min4(ray.tmax, max2(x0,x1), max2(y0,y1), max2(z0,z1));

  return (tminbox<=tmaxbox);
}
#endif
