#include "D_BVH.cuh"

#include <omp.h>

#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "D_AABB.cuh"
#include "D_Ray.cuh"
#include "Spheres.h"
#include "float3_util.cuh"
#include "cudasupport.h"

D_BVH *copyTreeToDevice(BVH * h_tree) {
 int numSpheres = h_tree->spheres.count();

 //we need to reformat the spheres data
 float4 *host_sphereData;
 host_sphereData = (float4 *) malloc(sizeof(float4)*numSpheres);
 for(int i=0; i<numSpheres; i++) {
  host_sphereData[i].x = h_tree->spheres.cx[i];
  host_sphereData[i].y = h_tree->spheres.cy[i];
  host_sphereData[i].z = h_tree->spheres.cz[i];
  host_sphereData[i].w = h_tree->spheres.invh2[i];
 }
 float4* device_sphereData;
 cudaMalloc(&device_sphereData, numSpheres*sizeof(float4));
 cudaMemcpy(device_sphereData, host_sphereData, numSpheres*sizeof(float4), cudaMemcpyHostToDevice);

 // create texture object
 cudaResourceDesc resDesc;
 memset(&resDesc, 0, sizeof(resDesc));
 resDesc.resType = cudaResourceTypeLinear;
 resDesc.res.linear.devPtr = device_sphereData;
 resDesc.res.linear.desc.f = cudaChannelFormatKindFloat;
 resDesc.res.linear.desc.x = 32; // bits per channel
 resDesc.res.linear.desc.y = 32;
 resDesc.res.linear.desc.z = 32;
 resDesc.res.linear.desc.w = 32;
 resDesc.res.linear.sizeInBytes = numSpheres*sizeof(float4);

 cudaTextureDesc texDesc;
 memset(&texDesc, 0, sizeof(texDesc));
 texDesc.readMode = cudaReadModeElementType;

 // create texture object: we only have to do this once!
 cudaTextureObject_t sphereTex;
 cudaCreateTextureObject(&sphereTex, &resDesc, &texDesc, NULL);

 //nodes are now a single texture with nNodes*2 float4 entries
 float4* host_nodeData;
 host_nodeData = (float4 *) malloc(sizeof(float4) * 2 * h_tree->nodes.size());

 for (int i = 0; i < h_tree->nodes.size(); ++i) {
  host_nodeData[i*2+0].x = h_tree->nodes[i].bbox.bounds[0].x();
  host_nodeData[i*2+0].y = h_tree->nodes[i].bbox.bounds[0].y();
  host_nodeData[i*2+0].z = h_tree->nodes[i].bbox.bounds[1].x();
  host_nodeData[i*2+0].w = h_tree->nodes[i].bbox.bounds[1].y();

  host_nodeData[i*2+1].x = h_tree->nodes[i].bbox.bounds[0].z();
  host_nodeData[i*2+1].y = h_tree->nodes[i].bbox.bounds[1].z();
  union convert {
   float f;
   int i;
  } data;
  if(h_tree->nodes[i].run!=0) { //is leaf
   data.i = h_tree->nodes[i].run;
   host_nodeData[i*2+1].z = data.f;
   data.i = h_tree->nodes[i].offset;
   host_nodeData[i*2+1].w = data.f;
  } else { //is not leaf
   data.i = h_tree->nodes[i].right;
   host_nodeData[i*2+1].z = data.f;
   data.i = -1;
   host_nodeData[i*2+1].w = data.f;
  }
 }

 float4* device_nodeData;
 cudaMalloc((void **) &(device_nodeData), sizeof(float4) * h_tree->nodes.size() * 2);
 cudaMemcpy(device_nodeData, host_nodeData, sizeof(float4) * h_tree->nodes.size() * 2, cudaMemcpyHostToDevice);

 // create texture object
 cudaResourceDesc resDesc2;
 memset(&resDesc2, 0, sizeof(resDesc2));
 resDesc2.resType = cudaResourceTypeLinear;
 resDesc2.res.linear.devPtr = device_nodeData;
 resDesc2.res.linear.desc.f = cudaChannelFormatKindFloat;
 resDesc2.res.linear.desc.x = 32; // bits per channel
 resDesc2.res.linear.desc.y = 32;
 resDesc2.res.linear.desc.z = 32;
 resDesc2.res.linear.desc.w = 32;
 resDesc2.res.linear.sizeInBytes = h_tree->nodes.size()*2*sizeof(float4);

 cudaTextureDesc texDesc2;
 memset(&texDesc2, 0, sizeof(texDesc2));
 texDesc2.readMode = cudaReadModeElementType;

 // create texture object: we only have to do this once!
 cudaTextureObject_t nodeTex;
 cudaCreateTextureObject(&nodeTex, &resDesc2, &texDesc2, NULL);

 D_BVH h_treeptr, *d_treeptr;
 cudaMalloc((void **) &d_treeptr, sizeof(D_BVH));
 h_treeptr.spheres = sphereTex;
 h_treeptr.nodes = nodeTex;
 h_treeptr.numSpheres = numSpheres;
 h_treeptr.device_sphereData = device_sphereData;
 h_treeptr.device_nodeData = device_nodeData;
 cudaMemcpy(d_treeptr, &h_treeptr, sizeof(D_BVH), cudaMemcpyHostToDevice);

 //free host memory used for temporary staging of device data
 free(host_sphereData);
 free(host_nodeData);

 return d_treeptr;
}

void freeTree(D_BVH * tree)
{
  //need to unbind textures then free memory
  D_BVH h_treeptr;
  cudaMemcpy(&h_treeptr, tree, sizeof(D_BVH), cudaMemcpyDeviceToHost);
  cudaDestroyTextureObject(h_treeptr.nodes);
  cudaDestroyTextureObject(h_treeptr.spheres);
  cudaFree(h_treeptr.device_sphereData);
  cudaFree(h_treeptr.device_nodeData);
  cudaFree(tree);
}
