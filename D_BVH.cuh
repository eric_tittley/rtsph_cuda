#pragma once

#include "D_Rays.cuh"
#include "Rays.h"
#include "BVH.h"
#include "Weight.cuh"
#include "cuda_constants.h"
#include <cudasupport.h>

#ifdef __CUDACC__
#include "float3_util.cuh"
#endif


/****************************************************
 * D_BVH stores three pointers to memory that has been bound to
 * cuda textures. spheres and nodes are arrays of float4, weights
 * are just float.
 *
 * The data for sphere i is found in spheres[i] and it is accessed by
 * float4 sphereData = tex1Dfetch<float4>(spheres, isphere);
 * sphereData.x = x co-ordinate of the center of the sphere
 * sphereData.y = y co-ordinate of the center of the sphere
 * sphereData.z = z co-ordinate of the center of the sphere
 * sphereData.w = invh2, 1/(radius^2). used for normalisation
 * 
 * The left child of the node is always its index+1.
 * The right child has its index stored
 *
 * The data for the bounding box of node i is accessed by
 * float4 xydata  = tex1Dfetch<float4>(nodes, i*2+0);
 * float4  zdata  = tex1Dfetch<float4>(nodes, i*2+1);
 * xydata.x       = x co-ordinate of corner 1 of the bounding box
 * xydata.y       = y co-ordinate of corner 1 of the bounding box
 * xydata.z       = x co-ordinate of the opposite corner of the bounding box
 * xydata.w       = y co-ordinate of the opposite corner of the bounding box
 *
 * zdata.x        = z co-ordinate of corner 1 of the bounding box
 * zdata.y        = z co-ordinate of the opposite corner of the bounding box
 *
 * the last 2 components depend on whether or not the node is a leaf
 * zdata.z (leaf)        = number of spheres in the leaf
 * zdata.z (not leaf)    = the index of the right child
 * zdata.w (leaf)        = the index in the spheres array of the first sphere
 *                         so to test all the spheres in a leaf we start from
 *                         zdata.w and iterate through zdata.z spheres
 * zdata.w (notleaf)     = -1, flag so we know it is not a leaf
 *
 * The weights array is for interpolating from a table. The square of the
 * normalised impact parameter (returned by intersect2) is used to interpolate
 * the SPH kernel integral which gives us the 'weight' of the sphere.
 *
 * float integral =  tex1D<float>(weights, nb2+OFFSET);
 *
 * the offset is needed due to the way cuda handles the normalised coordinates.
 * The syntax is slightly different (tex1D/tex1Dfetch) becuase weights are bound
 * to a cudaArray rather than linear memory. This is needed to use the 
 * normalised coordinates.
 * This way of interpolating uses the hardware texture filtering units which are
 * faster but slightly less accurate than doing linear interpolation in software. 
 ****************************************************/

struct D_BVH {
  static constexpr int STACK_SENTINEL = -128;
  static constexpr int WARP_SIZE = 32;
  static constexpr int STACK_SIZE = 32;
  static constexpr int WARPS_IN_BLOCK = (512/WARP_SIZE);
  static constexpr int MAX_PER_LEAF = 32;

  cudaTextureObject_t spheres; //float4
  cudaTextureObject_t nodes;   //float4 * 2
  unsigned int numSpheres;
  float4* device_nodeData; // Pointers to original data so it can be cleaned up again
  float4* device_sphereData;

  template <typename Func>
  ERT_DEVICE void visitRayIntersects(D_Ray ray, Func visit) const;
};

ERT_HOST D_BVH* copyTreeToDevice(BVH* h_tree);
ERT_HOST void freeTree(D_BVH*);

#ifdef __CUDACC__
template <typename Func>
__device__ void D_BVH::visitRayIntersects(const D_Ray ray, Func visit) const {
 //const int lane = threadIdx.x % WARP_SIZE;
 const int wid = threadIdx.x / WARP_SIZE;
 __shared__ int stack[STACK_SIZE * WARPS_IN_BLOCK];
 int * stack_ptr = stack + STACK_SIZE * wid; //warps use the same stack
 *stack_ptr = STACK_SENTINEL;
 //could now theoretically loop over rays here i.e. persistent threads
 float3 OoD;
 OoD.x = ray.origin.x*ray.invdir.x;
 OoD.y = ray.origin.y*ray.invdir.y;
 OoD.z = ray.origin.z*ray.invdir.z;
 stack_ptr++;
 *stack_ptr = 0; //push root to stack

 while (*stack_ptr > STACK_SENTINEL) {           //while stack is not empty
  int currentNode = *stack_ptr;
  --stack_ptr;
  // fetch the BVH node
  float4 n1xy = tex1Dfetch<float4>(nodes, currentNode * 2 + 0);
  float4 n1z = tex1Dfetch<float4>(nodes, currentNode * 2 + 1);
  const float4 & nodeData = n1z;
  // intersection tests
  float x10 = n1xy.x * ray.invdir.x - OoD.x;
  float y10 = n1xy.y * ray.invdir.y - OoD.y;
  float z10 = n1z.x * ray.invdir.z - OoD.z;
  float x11 = n1xy.z * ray.invdir.x - OoD.x;
  float y11 = n1xy.w * ray.invdir.y - OoD.y;
  float z11 = n1z.y * ray.invdir.z - OoD.z;
  // test if bounding box has been intersected
  float tminbox1 = max4(0, min2(x10, x11), min2(y10, y11), min2(z10, z11));
  float tmaxbox1 =
      min4(ray.tmax, max2(x10, x11), max2(y10, y11), max2(z10, z11));
  bool overlap1 = tminbox1 <= tmaxbox1;

  if (__any_sync(__activemask(), overlap1)) {
    if (__float_as_int(nodeData.w) == -1) {
      int child1 = currentNode + 1;  // left child is always i+1
      int child2 = __float_as_int(nodeData.z);
      stack_ptr++;
      *stack_ptr = child2;
      stack_ptr++;
      *stack_ptr = child1;
    } else {
      for (int i = 0; i < __float_as_int(nodeData.z); i++) {
        int isphere = __float_as_int(nodeData.w) + i;
        float4 sphereData = tex1Dfetch<float4>(this->spheres, isphere);
        auto origin = make_float3(sphereData.x, sphereData.y, sphereData.z);
        auto invh2 = sphereData.w;
        visit(isphere, origin, invh2);
      }
    }
  }
 }
}
#endif
