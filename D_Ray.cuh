#pragma once
/// \file Ray.h
/// \brief Ray & RayW containers

#include "Common.h"


///
///
///
struct D_Ray {
    ///
    float3 origin;

    ///
    float3 direction;

    ///
    float3 invdir;

    ///
    float tmax;

    ///
    unsigned int id;
};

