#include "D_Rays.cuh"

__host__
D_Rays* copyRaysToDevice(Rays const * h_rays) {
	size_t d_rayssize = sizeof(D_Rays);
	D_Rays *h_raysptr = (D_Rays*)malloc(sizeof(D_Rays)), *d_raysptr;

	printf("copy rays\n");
	//copy rays
	cudaMalloc((void**) &d_raysptr, d_rayssize);

	int numRays = h_rays->ox.size();
	
	const int num_fields = 10;

	float *data_block;

	//allocate block of memory for whole rays object
	cudaMalloc((void**) &(data_block), sizeof(float)*numRays*num_fields);
	//then copy rays data to sections of the block
	cudaMemcpy(&data_block[0], &h_rays->ox[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays], &h_rays->oy[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*2], &h_rays->oz[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);

	cudaMemcpy(&data_block[numRays*3], &h_rays->dx[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*4], &h_rays->dy[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*5], &h_rays->dz[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);

	cudaMemcpy(&data_block[numRays*6], &h_rays->ivdx[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*7], &h_rays->ivdy[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*8], &h_rays->ivdz[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);

	cudaMemcpy(&data_block[numRays*9], &h_rays->tmax[0], sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(&data_block[numRays*10], &h_rays->id[0], sizeof(unsigned int)*numRays, cudaMemcpyHostToDevice);

	cudaError_t cudaerr = cudaDeviceSynchronize();

	h_raysptr->ox   = 		  &data_block[0];
	h_raysptr->oy   = 		  &data_block[numRays];
	h_raysptr->oz   = 		  &data_block[numRays*2];
	h_raysptr->dx   = 		  &data_block[numRays*3];
	h_raysptr->dy   = 		  &data_block[numRays*4];
	h_raysptr->dz   = 		  &data_block[numRays*5];
	h_raysptr->ivdx = 		  &data_block[numRays*6];
	h_raysptr->ivdy = 		  &data_block[numRays*7];
	h_raysptr->ivdz = 		  &data_block[numRays*8];
	h_raysptr->tmax = 		  &data_block[numRays*9];
	h_raysptr->id   = (unsigned int *)&data_block[numRays*10];
	h_raysptr->size = (unsigned int  ) numRays;

	cudaMemcpy(d_raysptr, h_raysptr, d_rayssize, cudaMemcpyHostToDevice);
	cudaerr = cudaDeviceSynchronize();

	return d_raysptr;
}

D_Rays * allocateRaysOnDevice(size_t const numRays) {

        D_Rays rays_h;

	cudaMalloc((void**) &(rays_h.ox),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.oy),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.oz),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.dx),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.dy),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.dz),   sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.ivdx), sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.ivdy), sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.ivdz), sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.tmax), sizeof(float)*numRays);
	cudaMalloc((void**) &(rays_h.id),   sizeof(unsigned int)*numRays);
        rays_h.size = (unsigned int)numRays;

        /* The structure on the device.  It will not be destroyed when this
	 * function returns.
	 */
        D_Rays * rays_d;
	cudaMalloc((void**) &rays_d, sizeof(D_Rays));
	cudaMemcpy(rays_d, &rays_h, sizeof(D_Rays), cudaMemcpyHostToDevice);
	return rays_d;
}


__host__
void copyRaysToDeviceWithoutAlloc(Rays * h_rays,
                                  D_Rays * d_rays) {
	size_t numRays = (size_t)h_rays->ox.size();

        /* Need a host-side copy of the structure which is a list of pointers
	 * to the data in device memory.
	 */
	D_Rays d_rays_h;
	cudaMemcpy(&d_rays_h, d_rays, sizeof(D_Rays), cudaMemcpyDeviceToHost);
	cudaMemcpy(d_rays_h.ox,   &(h_rays->ox[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.oy,   &(h_rays->oy[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.oz,   &(h_rays->oz[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.dx,   &(h_rays->dx[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.dy,   &(h_rays->dy[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.dz,   &(h_rays->dz[0]),   sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.ivdx, &(h_rays->ivdx[0]), sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.ivdy, &(h_rays->ivdy[0]), sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.ivdz, &(h_rays->ivdz[0]), sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.tmax, &(h_rays->tmax[0]), sizeof(float)*numRays, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rays_h.id,   &(h_rays->id[0]),   sizeof(unsigned int)*numRays,   cudaMemcpyHostToDevice);

	// These last two lines are just to copy size back.  Every other
	// structure element is unchanged.
        d_rays_h.size = (unsigned int)h_rays->count();
	cudaMemcpy(d_rays, &d_rays_h, sizeof(D_Rays), cudaMemcpyHostToDevice);
}

__host__
void freeRays(D_Rays* rays) {
	D_Rays *h_rays = (D_Rays *) malloc(sizeof(D_Rays));
  	cudaMemcpy(h_rays, rays, sizeof(D_Rays), cudaMemcpyDeviceToHost);
	cudaFree((void *)h_rays->ox);
	cudaFree((void *)h_rays->oy);
	cudaFree((void *)h_rays->oz);
	cudaFree((void *)h_rays->dx);
	cudaFree((void *)h_rays->dy);
	cudaFree((void *)h_rays->dz);
	cudaFree((void *)h_rays->ivdx);
	cudaFree((void *)h_rays->ivdy);
	cudaFree((void *)h_rays->ivdz);
	cudaFree((void *)h_rays->tmax);
	cudaFree((void *)h_rays->id);
	cudaFree((void *)rays);
	free(h_rays);
}
