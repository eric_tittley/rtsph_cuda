#pragma once

#include "Rays.h"
#include "D_Ray.cuh"

//SOA
struct D_Rays {
	/// Origins of ray set.
	float *ox, *oy, *oz;

	/// Directions of ray set.
	float *dx, *dy, *dz;
	float *ivdx, *ivdy, *ivdz;

	/// tmin and tmax values of ray set
	float *tmax;

	unsigned int *id;
	unsigned int size;
};

__host__ D_Rays * allocateRaysOnDevice(size_t const numRays);
__host__ D_Rays * copyRaysToDevice(Rays const *);
__host__ void copyRaysToDeviceWithoutAlloc(Rays * h_rays,
                                           D_Rays * d_rays);
__host__ void freeRays(D_Rays *rays);
