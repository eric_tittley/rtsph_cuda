#include "D_BVH.cuh"

#include "IntegrateAlongRay.h"
#include "D_Ray.cuh"
#include "LinearBVH.cuh"
#include "float3_util.cuh"
#include "cudasupport.h"


#include "../libManitou_CUDA/Vec3.h"
#include "../libManitou_CUDA/cudaMemory.h"

namespace {

/*  Performs a ray-sphere intersection test and returns the square of the impact
 *  parameter.
 */

__device__ float intersect2(float3 sphere_origin, float sphere_invh2, const D_Ray ray) {
  sphere_origin -= ray.origin;
  float l2 = dot(sphere_origin, sphere_origin);

  //project onto direction
  float tb = dot(sphere_origin, ray.direction);

  //pythagoras
  float tb2 = tb*tb;
  float b2 = l2 - tb2;

  //square normalized impact parameter
  float nb2 = b2 * sphere_invh2;
  //impose roots & ray limits & filtering
  //1.0 will contribute 0.0 as kernel value
  return (nb2<=1.0f) && (0.0f<tb) && (tb<ray.tmax) ? nb2 : 1.0f;
}

template <typename Tree>
__device__ float findIntersects(
    const Tree & tree, Weights::DevInterface sph_weights, const D_Ray ray, const float * q) {
  float tmpWeight = 0.;
  tree.visitRayIntersects(ray,
    [&](int isphere, float3 origin, float invh2) {
      float nb2 = intersect2(origin, invh2, ray);
      float sum = sph_weights.getRayImpactWeight(nb2) * invh2;
      tmpWeight += sum * q[isphere];
    });
  return tmpWeight;
}

template <typename Tree>
__device__ float3 findIntersects(
    const Tree &tree, Weights::DevInterface sph_weights, const D_Ray ray, const float3 * q) {
  float3 tmpWeight = make_float3(0., 0., 0.);
  tree.visitRayIntersects(ray,
    [&](int isphere, float3 origin, float invh2) {
      float nb2 = intersect2(origin, invh2, ray);
      float sum = sph_weights.getRayImpactWeight(nb2) * invh2;
      tmpWeight += sum * q[isphere];
    });
 return tmpWeight;
}

template <typename T, typename Tree>
__global__ void findIntersectsKernel(const D_Rays * rays,
                                     const Tree tree,
                                     Weights::DevInterface sph_weights,
                                     T * weights,
                                     const T * q) {
 const int numRays = rays->size;
 const int idx = threadIdx.x + blockIdx.x * blockDim.x;
 const int span = gridDim.x * blockDim.x;
 int i;
 for (i = idx; i < numRays; i += span) {
  D_Ray ray;
  //load ray to thread local memory
  //TODO try shared memory
  ray.origin.x = rays->ox[i];
  ray.origin.y = rays->oy[i];
  ray.origin.z = rays->oz[i];

  ray.direction.x = rays->dx[i];
  ray.direction.y = rays->dy[i];
  ray.direction.z = rays->dz[i];

  ray.invdir.x = rays->ivdx[i];
  ray.invdir.y = rays->ivdy[i];
  ray.invdir.z = rays->ivdz[i];

  ray.tmax = rays->tmax[i];

  weights[i] = findIntersects(tree, sph_weights, ray, q);
 }
}

// Wrapper so we can call D_BVH * as if it wasn't a pointer
struct BVHWrapper {
  const D_BVH * tree;

  template <typename Func>
  __device__ void visitRayIntersects(D_Ray ray, Func func) const {
    tree->visitRayIntersects(ray, func);
  }
};

} // namespace (anonymous)

__host__ void launchFindIntersectsKernel(
    const D_Rays *rays,
    const D_BVH *tree,
    const Weights &sph_weights,
    float *weights,
    const float *q,
    size_t size) {
 //We need to know the block size at compile time so have to hard code these values
 int gridSize = 256;                  // The actual grid size needed, based on input size
 int blockSize = 512;                 // The launch configurator returned block size
// setGridAndBlockSize(size, (void *) findIntersectsKernel,
//                     &gridSize, &blockSize);
 findIntersectsKernel <<< gridSize, blockSize >>> (rays, BVHWrapper{tree}, sph_weights.device(), weights, q);
 cudaError_t cudaerr = cudaDeviceSynchronize();
#ifdef DEBUG
 if(cudaerr!=cudaSuccess) {
  printf("findIntersectsKernel error: \"%s\".\n", cudaGetErrorString(cudaerr));
 }
#endif
}

__host__ void launchFindIntersectsKernel(
    const D_Rays * rays,
    const LinearBVH * tree,
    const Weights & sph_weights,
    float *weights,
    const float *q,
    size_t const size) {
  //We need to know the block size at compile time so have to hard code these values
  int gridSize = 256;                  // The actual grid size needed, based on input size
  int blockSize = 512;                 // The launch configurator returned block size
// setGridAndBlockSize(size, (void *) findIntersectsKernel,
//                     &gridSize, &blockSize);
  findIntersectsKernel <<< gridSize, blockSize >>> (rays, tree->get_ref(), sph_weights.device(), weights, q);
  cudaError_t cudaerr = cudaDeviceSynchronize();
#ifdef DEBUG
  if(cudaerr!=cudaSuccess) {
    printf("findIntersectsKernel error: \"%s\".\n", cudaGetErrorString(cudaerr));
  }
#endif
}

__host__ void
launchFindIntersectsKernel3(const D_Rays * rays,
                            const D_BVH * tree,
                            const Weights & sph_weights,
                            float3 *weights,
                            const float3 *q,
                            size_t const size) {
 int gridSize = 128;                  // The actual grid size needed, based on input size
 int blockSize = 512;                 // The launch configurator returned block size
 // setGridAndBlockSize(size, (void *) findIntersectsKernel,
 //                     &gridSize, &blockSize);
 findIntersectsKernel <<< gridSize, blockSize >>> (rays, BVHWrapper{tree}, sph_weights.device(), weights, q);
 cudaError_t cudaerr = cudaDeviceSynchronize();
#ifdef DEBUG
 if(cudaerr!=cudaSuccess) {
  printf("findIntersectsKernel error: \"%s\".\n", cudaGetErrorString(cudaerr));
 }
#endif
}

__host__ void launchFindIntersectsKernel3(
    const D_Rays * rays,
    const LinearBVH * tree,
    const Weights & sph_weights,
    float3 *weights,
    const float3 *q,
    size_t const size) {
  int gridSize = 128;                  // The actual grid size needed, based on input size
  int blockSize = 512;                 // The launch configurator returned block size
  // setGridAndBlockSize(size, (void *) findIntersectsKernel,
  //                     &gridSize, &blockSize);
  findIntersectsKernel <<< gridSize, blockSize >>> (rays, tree->get_ref(), sph_weights.device(), weights, q);
  cudaError_t cudaerr = cudaDeviceSynchronize();
#ifdef DEBUG
  if(cudaerr!=cudaSuccess) {
    printf("findIntersectsKernel error: \"%s\".\n", cudaGetErrorString(cudaerr));
  }
#endif
}
