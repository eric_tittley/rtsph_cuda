#include <cuda_runtime.h>
#include <cudasupport.h>
#include <vector_types.h>

#include "Weight.cuh"

// fwd:
struct D_BVH;
struct D_Rays;
struct LinearBVH;

/** Weighted column density integral along a ray */
ERT_HOST void launchFindIntersectsKernel(const D_Rays *rays,
                                         const D_BVH *tree,
                                         const Weights &sph_weights,
                                         float *weights,
                                         const float *q,
                                         size_t size);
ERT_HOST void launchFindIntersectsKernel(const D_Rays *rays,
                                         const LinearBVH *tree,
                                         const Weights &sph_weights,
                                         float *weights,
                                         const float *q,
                                         size_t size);

/** Weighted column density integral along a ray */
ERT_HOST void launchFindIntersectsKernel3(const D_Rays *rays,
                                          const D_BVH *tree,
                                          const Weights &sph_weights,
                                          float3 *weights,
                                          const float3 *q,
                                          size_t size);
ERT_HOST void launchFindIntersectsKernel3(const D_Rays *rays,
                                          const LinearBVH *tree,
                                          const Weights &sph_weights,
                                          float3 *weights,
                                          const float3 *q,
                                          size_t size);
