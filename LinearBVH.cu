#include "LinearBVH.cuh"

#include <limits>
#include <float.h> // for FLT_RADIX
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
#include <thrust/transform_reduce.h>
#include <thrust/device_ptr.h>
#include <thrust/remove.h>
#include <cstdint>
#include <vector>
#include <cuda/functional> // for cuda::proclaim_return_type

#include "../libManitou_CUDA/Vec3.h"
#include "../libManitou_CUDA/cudaMemory.h"
#include <cudasupport.h>

namespace {

constexpr int MAX_SPHERES_PER_LEAF = 32;
constexpr bool LBVH_DEBUG = true;
constexpr bool USE_MORTON_KEY = false;

const __device__ unsigned char rottable3[48][8] = {
  {36, 28, 25, 27, 10, 10, 25, 27},
  {29, 11, 24, 24, 37, 11, 26, 26},
  {8, 8, 25, 27, 30, 38, 25, 27},
  {9, 39, 24, 24, 9, 31, 26, 26},
  {40, 24, 44, 32, 40, 6, 44, 6},
  {25, 7, 33, 7, 41, 41, 45, 45},
  {4, 42, 4, 46, 26, 42, 34, 46},
  {43, 43, 47, 47, 5, 27, 5, 35},
  {33, 35, 36, 28, 33, 35, 2, 2},
  {32, 32, 29, 3, 34, 34, 37, 3},
  {33, 35, 0, 0, 33, 35, 30, 38},
  {32, 32, 1, 39, 34, 34, 1, 31},
  {24, 42, 32, 46, 14, 42, 14, 46},
  {43, 43, 47, 47, 25, 15, 33, 15},
  {40, 12, 44, 12, 40, 26, 44, 34},
  {13, 27, 13, 35, 41, 41, 45, 45},
  {28, 41, 28, 22, 38, 43, 38, 22},
  {42, 40, 23, 23, 29, 39, 29, 39},
  {41, 36, 20, 36, 43, 30, 20, 30},
  {37, 31, 37, 31, 42, 40, 21, 21},
  {28, 18, 28, 45, 38, 18, 38, 47},
  {19, 19, 46, 44, 29, 39, 29, 39},
  {16, 36, 45, 36, 16, 30, 47, 30},
  {37, 31, 37, 31, 17, 17, 46, 44},
  {12, 4, 1, 3, 34, 34, 1, 3},
  {5, 35, 0, 0, 13, 35, 2, 2},
  {32, 32, 1, 3, 6, 14, 1, 3},
  {33, 15, 0, 0, 33, 7, 2, 2},
  {16, 0, 20, 8, 16, 30, 20, 30},
  {1, 31, 9, 31, 17, 17, 21, 21},
  {28, 18, 28, 22, 2, 18, 10, 22},
  {19, 19, 23, 23, 29, 3, 29, 11},
  {9, 11, 12, 4, 9, 11, 26, 26},
  {8, 8, 5, 27, 10, 10, 13, 27},
  {9, 11, 24, 24, 9, 11, 6, 14},
  {8, 8, 25, 15, 10, 10, 25, 7},
  {0, 18, 8, 22, 38, 18, 38, 22},
  {19, 19, 23, 23, 1, 39, 9, 39},
  {16, 36, 20, 36, 16, 2, 20, 10},
  {37, 3, 37, 11, 17, 17, 21, 21},
  {4, 17, 4, 46, 14, 19, 14, 46},
  {18, 16, 47, 47, 5, 15, 5, 15},
  {17, 12, 44, 12, 19, 6, 44, 6},
  {13, 7, 13, 7, 18, 16, 45, 45},
  {4, 42, 4, 21, 14, 42, 14, 23},
  {43, 43, 22, 20, 5, 15, 5, 15},
  {40, 12, 21, 12, 40, 6, 23, 6},
  {13, 7, 13, 7, 41, 41, 22, 20}
};

const __device__ unsigned char subpix3[48][8] = {
  {0, 7, 1, 6, 3, 4, 2, 5},
  {7, 4, 6, 5, 0, 3, 1, 2},
  {4, 3, 5, 2, 7, 0, 6, 1},
  {3, 0, 2, 1, 4, 7, 5, 6},
  {1, 0, 6, 7, 2, 3, 5, 4},
  {0, 3, 7, 4, 1, 2, 6, 5},
  {3, 2, 4, 5, 0, 1, 7, 6},
  {2, 1, 5, 6, 3, 0, 4, 7},
  {6, 1, 7, 0, 5, 2, 4, 3},
  {1, 2, 0, 3, 6, 5, 7, 4},
  {2, 5, 3, 4, 1, 6, 0, 7},
  {5, 6, 4, 7, 2, 1, 3, 0},
  {7, 6, 0, 1, 4, 5, 3, 2},
  {6, 5, 1, 2, 7, 4, 0, 3},
  {5, 4, 2, 3, 6, 7, 1, 0},
  {4, 7, 3, 0, 5, 6, 2, 1},
  {6, 7, 5, 4, 1, 0, 2, 3},
  {7, 0, 4, 3, 6, 1, 5, 2},
  {0, 1, 3, 2, 7, 6, 4, 5},
  {1, 6, 2, 5, 0, 7, 3, 4},
  {2, 3, 1, 0, 5, 4, 6, 7},
  {3, 4, 0, 7, 2, 5, 1, 6},
  {4, 5, 7, 6, 3, 2, 0, 1},
  {5, 2, 6, 1, 4, 3, 7, 0},
  {7, 0, 6, 1, 4, 3, 5, 2},
  {0, 3, 1, 2, 7, 4, 6, 5},
  {3, 4, 2, 5, 0, 7, 1, 6},
  {4, 7, 5, 6, 3, 0, 2, 1},
  {6, 7, 1, 0, 5, 4, 2, 3},
  {7, 4, 0, 3, 6, 5, 1, 2},
  {4, 5, 3, 2, 7, 6, 0, 1},
  {5, 6, 2, 1, 4, 7, 3, 0},
  {1, 6, 0, 7, 2, 5, 3, 4},
  {6, 5, 7, 4, 1, 2, 0, 3},
  {5, 2, 4, 3, 6, 1, 7, 0},
  {2, 1, 3, 0, 5, 6, 4, 7},
  {0, 1, 7, 6, 3, 2, 4, 5},
  {1, 2, 6, 5, 0, 3, 7, 4},
  {2, 3, 5, 4, 1, 0, 6, 7},
  {3, 0, 4, 7, 2, 1, 5, 6},
  {1, 0, 2, 3, 6, 7, 5, 4},
  {0, 7, 3, 4, 1, 6, 2, 5},
  {7, 6, 4, 5, 0, 1, 3, 2},
  {6, 1, 5, 2, 7, 0, 4, 3},
  {5, 4, 6, 7, 2, 3, 1, 0},
  {4, 3, 7, 0, 5, 2, 6, 1},
  {3, 2, 0, 1, 4, 5, 7, 6},
  {2, 5, 1, 6, 3, 4, 0, 7}
};

__device__ uint32_t flt_to_int_10(float x) {
  if (x <= 0.f) { return 0; }
  if (x >= 1.f) { return (1u << 10u) - 1u; }

  static_assert(FLT_RADIX == 2);
  auto res = (uint32_t) std::scalbn(x, 10);  // x * 2^10
  assert(res < (1 << 10)); // Must fit in 10 bits
  return res;
}

/* Compute the Peano-Hilbert key for a normalised float triplet (x,y,z) */
__device__ uint32_t peano_hilbert_key(float x_, float y_, float z_)
{
  int x = flt_to_int_10(x_);
  int y = flt_to_int_10(y_);
  int z = flt_to_int_10(z_);

  unsigned char rotation = 0;
  uint32_t key = 0;
  constexpr int bits = 10;

  for(int mask = 1 << (bits - 1); mask > 0; mask >>= 1)
    {
      unsigned char pix = ((x & mask) ? 4 : 0) | ((y & mask) ? 2 : 0) | ((z & mask) ? 1 : 0);

      key <<= 3;
      key |= subpix3[rotation][pix];
      rotation = rottable3[rotation][pix];
    }

  return key;
}


// References:
// https://devblogs.nvidia.com/thinking-parallel-part-iii-tree-construction-gpu/
// https://devblogs.nvidia.com/wp-content/uploads/2012/11/karras2012hpg_paper.pdf

__device__ uint32_t expand_bits(uint32_t v)
{
  v = (v * 0x00010001u) & 0xFF0000FFu;
  v = (v * 0x00000101u) & 0x0F00F00Fu;
  v = (v * 0x00000011u) & 0xC30C30C3u;
  v = (v * 0x00000005u) & 0x49249249u;
  return v;
}

// Calculates a 30-bit Morton code for the
// given 3D point located within the unit cube [0,1].
__device__ uint32_t morton_3d(float x, float y, float z)
{
  uint32_t xx = expand_bits(flt_to_int_10(x));
  uint32_t yy = expand_bits(flt_to_int_10(y));
  uint32_t zz = expand_bits(flt_to_int_10(z));
  return xx * 4 + yy * 2 + zz;
}

__host__ void determine_particle_order(const LinearBVH::Sphere* particles,
                                        uint32_t* keys,
                                        uint32_t* index_map,
                                        uint32_t num_part) {
  // Generate morton keys
  auto inf = std::numeric_limits<float>::infinity();
  auto part = thrust::device_pointer_cast(particles);
  auto bounds = thrust::transform_reduce(
    part, part + num_part,
    [] __host__ __device__ (const LinearBVH::Sphere & sph) {
      return D_AABB{sph.pos, sph.pos};
    },
    D_AABB{{inf, inf, inf}, {-inf, -inf, -inf}},
    cuda::proclaim_return_type<D_AABB>(
      [] __device__ (const D_AABB & b1, const D_AABB & b2) {
        return D_AABB::bb_union(b1, b2);
      }
    )
  );

  float3 spans = bounds.max - bounds.min;
  float3 scale = make_float3(1.f / spans.x, 1.f / spans.y, 1.f / spans.z);

  if (USE_MORTON_KEY) {
    thrust::transform(
        part, part + num_part, keys,
        cuda::proclaim_return_type<uint32_t>(
          [=] __device__ (const LinearBVH::Sphere & part) {
            auto dr = part.pos - bounds.min;
            return morton_3d(dr.x * scale.x, dr.y * scale.y, dr.z * scale.z);
          }
        )
    );
  } else {
    thrust::transform(
        part, part + num_part, keys,
        cuda::proclaim_return_type<uint32_t>(
        [=] __device__ (const LinearBVH::Sphere & part) {
          auto dr = part.pos - bounds.min;
          return peano_hilbert_key(dr.x * scale.x, dr.y * scale.y, dr.z * scale.z);
        })
    );
    
  }

  // Generate index_map and sort keys
  auto idx_map = thrust::device_pointer_cast(index_map);
  thrust::sequence(idx_map, idx_map + num_part, uint32_t(0));
  auto d_keys = thrust::device_pointer_cast(keys);
  thrust::sort_by_key(d_keys, d_keys + num_part, idx_map);
}

__host__ void rearrange_spheres(LinearBVH::Sphere* out,
                                const LinearBVH::Sphere* in,
                                uint32_t* map,
                                uint32_t num_spheres) {
  thrust::counting_iterator<uint32_t> iter(0);
  auto d_out = thrust::device_pointer_cast(out);
  thrust::transform(iter, iter + num_spheres, d_out,
      [=] __device__ (uint32_t i) {
        return in[map[i]];
      });
}

__device__ int common_prefix(uint32_t a, uint32_t b) {
  static_assert(sizeof(uint32_t) == sizeof(int), "");
  // a ^ b is a mask of the bits that differ
  // "count leading zeros" gives the number of leading bits that match
  return __clz((int) (a ^ b));
}

__device__ unsigned midpoint(unsigned a, unsigned b) {
  // equivalent to (a + b) / 2 but without overflow
  return __uhadd(a, b);
}

__device__ thrust::pair<uint32_t, uint32_t> determine_range(const uint32_t * keys, uint32_t num_obj, uint32_t idx) {

  const auto code = keys[idx];
  auto delta = [&](uint32_t shift, int dir_) {
      // Calculate idx + dir_ * shift in unsigned and checking for overflow
      uint32_t check_idx;
      if (dir_ > 0) {
        if (shift >= num_obj - idx) {
          return -1;
        }
        check_idx = idx + shift;
      } else {
        if (shift > idx) {
          return -1;
        }
        check_idx = idx - shift;
      }

      if (keys[check_idx] == code) {
        return 32 + common_prefix(idx, check_idx);
      }
      return common_prefix(code, keys[check_idx]);
    };

  const int dl = delta(1, -1);
  const int dr = delta(1, 1);
  const int dir = dr > dl ? 1 : -1;
  const int dmin = std::min(dl, dr);

  // Compute upper bound
  uint32_t lmax = 2;
  while (delta(lmax, dir) > dmin) {
    lmax *= 2;
  };

  //Compute lower bound
  uint32_t l = 0;
  for (auto t = lmax/2; t > 0; t /= 2) {
    if (delta(l + t, dir) > dmin) {
      l += t;
    }
  }
  if (!(l & lmax/2)) {
    printf("ERROR: idx=%6u, l=%6u, lmax=%6u, d(1)=%d, dmin=%d\n", idx, l, lmax, delta(1, dir), dmin);
  }

  if (dir > 0) {
    return {idx, idx + l};
  } else {
    return {idx - l, idx};
  }
}

__device__ uint32_t find_split(
    const uint32_t *keys, uint32_t first, uint32_t last) {
  auto delta = [&](uint32_t i, uint32_t j) {
      // We need to worry about duplicates
      if (keys[i] == keys[j]) {
        return 32 + common_prefix(i, j);
      }
      return common_prefix(keys[i], keys[j]);
    };

  uint32_t dnode = delta(first, last);

  // Use binary search to find where the next bit differs.
  // Specifically, we are looking for the highest object that
  // shares more than dnode bits with the first one.

  uint32_t split = first; // initial guess
  uint32_t step = last - first;

  do {
    step = midpoint(step, 1);     // exponential decrease
    uint32_t new_split = split + step;  // proposed new position

    if (new_split < last) {
      if (delta(first, new_split) > dnode) split = new_split;  // accept proposal
    }
  } while (step > 1);

  return split;
}

__global__ void generate_leaf_nodes(
    LinearBVH::LeafNode * leaf_nodes,
    const uint32_t * sorted_keys,
    uint32_t num_spheres) {
  const uint32_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const uint32_t ispan = blockDim.x * gridDim.x;
  for (uint32_t idx = istart; idx < num_spheres - 1; idx += ispan)
  {
    // Find out which range of objects the node corresponds to.
    // (This is where the magic happens!)
    const auto range = determine_range(sorted_keys, num_spheres, idx);
    const auto first = range.first, last = range.second;

    if (idx > 0 && last - first < MAX_SPHERES_PER_LEAF) {
      // This range is already covered by another leaf node
      continue;
    }

    // Determine where to split the range.
    uint32_t split = find_split(sorted_keys, first, last);

    // create left leaf
    if ((split + 1) - first <= MAX_SPHERES_PER_LEAF) {
      leaf_nodes[split].first_sphere = first;
      leaf_nodes[split].num_spheres = (split + 1) - first;
    }

    // create right leaf
    if (last - split <= MAX_SPHERES_PER_LEAF) {
      leaf_nodes[split + 1].first_sphere = split + 1;
      leaf_nodes[split + 1].num_spheres = last - split;
    }
  }
}

__global__ void generate_node_heirarchy(
    LinearBVH::InnerNode * inner_nodes,
    LinearBVH::LeafNode * leaf_nodes,
    const uint32_t * sorted_keys,
    uint32_t num_leaf_nodes) {
  const uint32_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const uint32_t ispan = blockDim.x * gridDim.x;
  for (uint32_t idx = istart; idx < num_leaf_nodes - 1; idx += ispan)
  {
    // Find out which range of objects the node corresponds to.
    // (This is where the magic happens!)
    const auto range = determine_range(sorted_keys, num_leaf_nodes, idx);
    const auto first = range.first, last = range.second;

    // Determine where to split the range.
    uint32_t split = find_split(sorted_keys, first, last);

    // link left child
    inner_nodes[idx].left_child = split;
    if (split == first) {
      inner_nodes[idx].flags = LinearBVH::left_leaf_flag;
      leaf_nodes[split].flags = LinearBVH::left_leaf_flag;
      leaf_nodes[split].parent = idx;
    } else {
      inner_nodes[idx].flags = 0;
      inner_nodes[split].parent = idx;
    }

    // link right child
    inner_nodes[idx].right_child = split + 1;
    if (split + 1 == last) {
      inner_nodes[idx].flags |= LinearBVH::right_leaf_flag;
      leaf_nodes[split + 1].flags = LinearBVH::right_leaf_flag;
      leaf_nodes[split + 1].parent = idx;
    } else {
      inner_nodes[split + 1].parent = idx;
    }
  }
}

__host__ uint32_t compact_leaf_nodes(
    uint32_t * keys,
    LinearBVH::LeafNode * leaf_nodes,
    uint32_t num_spheres) {
  // generate_leaf_nodes leaves unused leaf nodes in the array. Find the useful
  // leaf nodes and make them contiguous in the array.

  // By zipping the ranges, we also compact together the key values. Due to the
  // radix tree structure, it doesn't matter which sphere from the leaf node we
  // take the key from

  auto first_leaf = thrust::device_pointer_cast(leaf_nodes);
  auto first_key = thrust::device_pointer_cast(keys);
  auto first_zip = thrust::make_zip_iterator(thrust::make_tuple(first_leaf, first_key));
  auto last_zip = thrust::remove_if(
      first_zip, first_zip + num_spheres,
      [] __device__ (const thrust::tuple<LinearBVH::LeafNode, uint32_t> &pair) {
        return (thrust::get<0>(pair).num_spheres == 0);
      });

  const uint32_t num_leaves = last_zip - first_zip;
  return num_leaves;
}

__global__ void calc_leaf_node_bounding_boxes(
    LinearBVH::InnerNode * inner_nodes,
    const LinearBVH::LeafNode * leaf_nodes,
    const LinearBVH::Sphere * spheres,
    uint32_t num_leaves) {
  const uint32_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const uint32_t ispan = blockDim.x * gridDim.x;

  for (uint32_t idx = istart; idx < num_leaves; idx += ispan) {
    const auto & leaf = leaf_nodes[idx];
    assert(leaf.num_spheres > 0);
    auto my_bb = D_AABB::from_sphere(
      spheres[leaf.first_sphere].pos, spheres[leaf.first_sphere].radius);
    for (uint32_t isphere = 1; isphere < leaf.num_spheres; ++isphere) {
      const auto & sphere = spheres[leaf.first_sphere + isphere];
      my_bb = D_AABB::sphere_union(my_bb, sphere.pos, sphere.radius);
    }

    auto & parent = inner_nodes[leaf.parent];

    if (parent.left_child == idx) {
      // We are the left child node
      parent.left_bb = my_bb;
      atomicOr(&parent.flags, LinearBVH::left_bb_set_flag);
    } else {
      // We are the right child node
      assert(parent.right_child == idx);
      parent.right_bb = my_bb;
      atomicOr(&parent.flags, LinearBVH::right_bb_set_flag);
    }
  }
}

__global__ void calc_inner_node_bounding_boxes(
    LinearBVH::InnerNode * inner_nodes,
    const LinearBVH::LeafNode * leaf_nodes,
    uint32_t num_leaves) {
  const uint32_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const uint32_t ispan = blockDim.x * gridDim.x;

  for (uint32_t idx = istart; idx < num_leaves; idx += ispan) {
    auto parent_idx = leaf_nodes[idx].parent;
    const auto flags = inner_nodes[parent_idx].flags;
    // If the other child isn't a leaf, that child's bb might not be calculated yet
    if (!(flags & LinearBVH::left_leaf_flag) ||
        !(flags & LinearBVH::right_leaf_flag)) {
      continue;
    }
    if (inner_nodes[parent_idx].right_child == idx) {
      continue;
    }
    assert(inner_nodes[parent_idx].left_child == idx);

    while (parent_idx != 0) {  // 0 is the root node
      auto cur_idx = parent_idx;
      auto & cur_node = inner_nodes[cur_idx];
      parent_idx = cur_node.parent;

      const auto my_bb = D_AABB::bb_union(cur_node.left_bb, cur_node.right_bb);

      auto & parent = inner_nodes[parent_idx];
      if (parent.left_child == cur_idx) {
        // We are the left child node
        parent.left_bb = my_bb;
        __threadfence();
        const auto old_flags = atomicOr(&parent.flags, LinearBVH::left_bb_set_flag);
        if (!(old_flags & LinearBVH::right_bb_set_flag)) {
          break;
        }
      } else {
        // We are the right child node
        assert(parent.right_child == cur_idx);
        parent.right_bb = my_bb;
        __threadfence();
        const auto old_flags = atomicOr(&parent.flags, LinearBVH::right_bb_set_flag);
        if (!(old_flags & LinearBVH::left_bb_set_flag)) {
          break;
        }
      }
    }
  }
}

__host__ void sphere_radii_to_invh2(LinearBVH::Sphere * spheres, uint32_t num_spheres) {
  auto sph = thrust::device_pointer_cast(spheres);
  thrust::for_each(sph, sph + num_spheres,
      [] __device__ (LinearBVH::Sphere & sphere) {
        auto h = sphere.radius;
        sphere.radius = 1.f / (h * h);
      });
}

} // namespace (anonymous)

LinearBVH::LinearBVH(uint32_t num_spheres) :
    num_spheres_(num_spheres),
    num_inner_nodes_(0),
    num_leaf_nodes_(num_spheres) {
  ERT_CUDA_CHECK(cudaMalloc(&leaf_nodes_, sizeof(*leaf_nodes_) * num_leaf_nodes_));
  ERT_CUDA_CHECK(cudaMalloc(&spheres_, sizeof(*spheres_) * num_spheres_));
  ERT_CUDA_CHECK(cudaMalloc(&map_, sizeof(*map_) * num_spheres_));
}

LinearBVH::LinearBVH(LinearBVH && other):
    inner_nodes_(other.inner_nodes_),
    leaf_nodes_(other.leaf_nodes_),
    spheres_(other.spheres_),
    map_(other.map_),
    num_spheres_(other.num_spheres_),
    num_inner_nodes_(other.num_inner_nodes_),
    num_leaf_nodes_(other.num_leaf_nodes_){
  other.inner_nodes_ = nullptr;
  other.leaf_nodes_ = nullptr;
  other.spheres_ = nullptr;
  other.map_ = nullptr;
  other.num_inner_nodes_ = 0;
  other.num_spheres_ = 0;
}


LinearBVH::~LinearBVH() {
  ERT_CUDA_CHECK(cudaFree(inner_nodes_));
  ERT_CUDA_CHECK(cudaFree(leaf_nodes_));
  ERT_CUDA_CHECK(cudaFree(spheres_));
  ERT_CUDA_CHECK(cudaFree(map_));
}


void LinearBVH::construct_from_host_spheres(LinearBVH& bvh, const LinearBVH::Sphere * host_spheres, uint32_t num_spheres) {
  auto dev_spheres = cuda_malloc<Sphere>(num_spheres);
  cudaMemcpy(dev_spheres.get(), host_spheres, sizeof(Sphere) * num_spheres, cudaMemcpyHostToDevice);
  construct_from_dev_spheres(bvh, dev_spheres.get(), num_spheres);
}

void LinearBVH::construct_from_dev_spheres(LinearBVH& bvh, const LinearBVH::Sphere * dev_spheres, uint32_t num_spheres) {
  bvh.build_tree(dev_spheres);
  bvh.validate_tree();
  // Convert radius to invh2 for use in ray tracing
  sphere_radii_to_invh2(bvh.spheres_, bvh.num_spheres_);
}

void LinearBVH::build_tree(const Sphere * in_spheres) {

  auto key_array = cuda_malloc<uint32_t>(num_spheres_);
  determine_particle_order(in_spheres, key_array.get(), map_, num_spheres_);
  rearrange_spheres(spheres_, in_spheres, map_, num_spheres_);

  // leaves must be zero-initialized as generate_leaf_nodes doesn't touch all of them
  cudaMemset(leaf_nodes_, 0, sizeof(*leaf_nodes_) * num_spheres_);

  int grid_size, block_size;
  setGridAndBlockSize(num_spheres_, (void*)&generate_leaf_nodes, &grid_size, &block_size);
  generate_leaf_nodes<<<grid_size, block_size>>>(leaf_nodes_, key_array.get(), num_spheres_);

  num_leaf_nodes_ = compact_leaf_nodes(key_array.get(), leaf_nodes_, num_spheres_);
  num_inner_nodes_ = num_leaf_nodes_ > 1 ? num_leaf_nodes_ - 1 : 0;
  ERT_CUDA_CHECK(cudaMalloc(&inner_nodes_, sizeof(InnerNode) * num_inner_nodes_));

  setGridAndBlockSize(num_leaf_nodes_, (void*)&generate_node_heirarchy, &grid_size, &block_size);
  generate_node_heirarchy<<<grid_size, block_size>>>(
      inner_nodes_, leaf_nodes_, key_array.get(), num_leaf_nodes_);

  setGridAndBlockSize(num_spheres_, (void*)&calc_leaf_node_bounding_boxes, &grid_size, &block_size);
  calc_leaf_node_bounding_boxes<<<grid_size, block_size>>>(
      inner_nodes_, leaf_nodes_, spheres_, num_leaf_nodes_);

  setGridAndBlockSize(num_spheres_, (void*)&calc_inner_node_bounding_boxes, &grid_size, &block_size);
  calc_inner_node_bounding_boxes<<<grid_size, block_size>>>(
      inner_nodes_, leaf_nodes_, num_leaf_nodes_);

  ERT_CUDA_CHECK(cudaDeviceSynchronize());
}

static void do_validate_tree(
  LinearBVH::InnerNode * inner_nodes,
  LinearBVH::LeafNode * leaf_nodes,
  uint32_t num_leaves,
  LinearBVH::Sphere * spheres,
  uint32_t num_spheres) {
  thrust::counting_iterator<uint32_t> iter(0);
  // Each sphere is in exactly one leaf node
  thrust::for_each(
    iter, iter + num_leaves,
    [=] __device__ (uint32_t idx) {
      const auto & my_node = leaf_nodes[idx];
      if (idx == 0) {
        assert(my_node.first_sphere == 0);
      }
      if (idx + 1 < num_leaves) {
        assert(my_node.first_sphere + my_node.num_spheres == leaf_nodes[idx + 1].first_sphere);
      } else {
        assert(my_node.first_sphere + my_node.num_spheres == num_spheres);
      }
    });

  // Each sphere is enclosed by the leaf node's bounding box
  auto leaves = thrust::device_pointer_cast(leaf_nodes);
  thrust::for_each(
    leaves, leaves + num_leaves,
    [=] __device__ (const LinearBVH::LeafNode & leaf) {
      const auto & parent = inner_nodes[leaf.parent];
      auto my_bb = (leaf.flags & LinearBVH::left_leaf_flag) ? parent.left_bb : parent.right_bb;
      for (int isphere = 0; isphere < leaf.num_spheres; ++isphere) {
        const auto & sphere = spheres[leaf.first_sphere + isphere];
        auto r = sphere.radius;
        auto dmin = sphere.pos - my_bb.min;
        if (!(dmin.x >= r && dmin.y >= r && dmin.z >= r)) {
          printf("[%d, %d]: r=%g dmin=(%6g, %6g, %6g)\n",
                 blockIdx.x, threadIdx.x, r, dmin.x, dmin.y, dmin.z);
          assert(false);
        }

        auto dmax = my_bb.max - sphere.pos;
        if (!(dmax.x >= r && dmax.y >= r && dmax.z >= r)) {
          printf("[%d, %d]: r=%g, dmax=(%6g, %6g, %6g)\n",
                 blockIdx.x, threadIdx.x, r, dmax.x, dmax.y, dmax.z);
          assert(false);
        }
      }
    });

  // Each inner node is enclosed by its parent
  thrust::for_each(
    leaves, leaves + num_leaves,
    [=] __device__ (const LinearBVH::LeafNode & leaf) {
      uint32_t cur_id = leaf.parent;

      for (int depth = 0; cur_id != 0; ++depth) {
        const auto & cur_node = inner_nodes[cur_id];
        const auto & parent = inner_nodes[cur_node.parent];

        const bool is_left = parent.left_child == cur_id;
        assert(is_left || parent.right_child == cur_id);

        auto my_bb = is_left ? parent.left_bb : parent.right_bb;
        if (
          (cur_node.left_bb.min.x - my_bb.min.x < 0) ||
          (cur_node.left_bb.min.y - my_bb.min.y < 0) ||
          (cur_node.left_bb.min.z - my_bb.min.z < 0) ||
          (cur_node.left_bb.max.x - my_bb.max.x > 0) ||
          (cur_node.left_bb.max.y - my_bb.max.y > 0) ||
          (cur_node.left_bb.max.z - my_bb.max.z > 0)) {
          printf(
            "[%u, %u] Left BB check failed (depth=%d, child=%u)\n"
            "  my_bb: {{%.10f, %.10f, %.10f}, {%.10f, %.10f, %.10f}}\n"
            "   left: {{%.10f, %.10f, %.10f}, {%.10f, %.10f, %.10f}}\n"
            "   diff: {{%.20e, %.20e, %.20e},\n"
            "          {%.20e, %.20e, %.20e}}\n\n",
            cur_id, cur_node.parent, depth, cur_node.left_child,
            my_bb.min.x, my_bb.min.y, my_bb.min.z,
            my_bb.max.x, my_bb.max.y, my_bb.max.z,
            cur_node.left_bb.min.x, cur_node.left_bb.min.y, cur_node.left_bb.min.z,
            cur_node.left_bb.max.x, cur_node.left_bb.max.y, cur_node.left_bb.max.z,
            cur_node.left_bb.min.x - my_bb.min.x, cur_node.left_bb.min.y - my_bb.min.y, cur_node.left_bb.min.z - my_bb.min.z,
            cur_node.left_bb.max.x - my_bb.max.x, cur_node.left_bb.max.y - my_bb.max.y, cur_node.left_bb.max.z - my_bb.max.z);
          assert(false);
        }

        if (
          (cur_node.right_bb.min.x - my_bb.min.x < 0) ||
          (cur_node.right_bb.min.y - my_bb.min.y < 0) ||
          (cur_node.right_bb.min.z - my_bb.min.z < 0) ||
          (cur_node.right_bb.max.x - my_bb.max.x > 0) ||
          (cur_node.right_bb.max.y - my_bb.max.y > 0) ||
          (cur_node.right_bb.max.z - my_bb.max.z > 0)) {
          printf(
            "[%u, %u] Right BB check failed (depth=%d, child=%u)\n"
            "  my_bb: {{%.10f, %.10f, %.10f}, {%.10f, %.10f, %.10f}}\n"
            "  right: {{%.10f, %.10f, %.10f}, {%.10f, %.10f, %.10f}}\n"
            "   diff: {{%.20e, %.20e, %.20e},\n"
            "          {%.20e, %.20e, %.20e}}\n\n",
            cur_id, cur_node.parent, depth, cur_node.left_child,
            my_bb.min.x, my_bb.min.y, my_bb.min.z,
            my_bb.max.x, my_bb.max.y, my_bb.max.z,
            cur_node.right_bb.min.x, cur_node.right_bb.min.y, cur_node.right_bb.min.z,
            cur_node.right_bb.max.x, cur_node.right_bb.max.y, cur_node.right_bb.max.z,
            cur_node.right_bb.min.x - my_bb.min.x, cur_node.right_bb.min.y - my_bb.min.y, cur_node.right_bb.min.z - my_bb.min.z,
            cur_node.right_bb.max.x - my_bb.max.x, cur_node.right_bb.max.y - my_bb.max.y, cur_node.right_bb.max.z - my_bb.max.z);
          assert(false);
        }

        if (!is_left) {
          return;
        }

        cur_id = cur_node.parent;
      }
    });
}

void LinearBVH::validate_tree() const {
  if (LBVH_DEBUG) {
    do_validate_tree(inner_nodes_, leaf_nodes_, num_leaf_nodes_, spheres_, num_spheres_);
  }
}

LinearBVH::Ref::Ref(const LinearBVH & tree):
    inner_nodes_(tree.inner_nodes_),
    leaf_nodes_(tree.leaf_nodes_),
    spheres_(tree.spheres_),
    map_(tree.map_),
    num_spheres_(tree.num_spheres_),
    num_inner_nodes_(tree.num_inner_nodes_),
    num_leaf_nodes_(tree.num_leaf_nodes_){
}


LinearBVH::Ref LinearBVH::get_ref() const {
  return Ref(*this);
}

