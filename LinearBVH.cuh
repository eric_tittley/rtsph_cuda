#pragma once
#include <cstdint>

#include "D_AABB.cuh"

#ifdef __CUDACC__
#include "D_Ray.cuh"
#include "float3_util.cuh"
#endif

class LinearBVH {
public:

  struct alignas(16) Sphere {
    float3 pos;
    float radius;
  };

  enum Flags : uint32_t {
    left_leaf_flag = 1u << 0u,
    right_leaf_flag = 1u << 1u,
    left_bb_set_flag = 1u << 2u,
    right_bb_set_flag = 1u << 3u,
  };

  struct alignas(16) LeafNode {
    uint32_t parent, flags;
    uint32_t first_sphere, num_spheres;
  };

  struct alignas(32) InnerNode {
    uint32_t parent, flags;
    uint32_t left_child, right_child;
    D_AABB left_bb, right_bb;
  };

  ~LinearBVH();
  LinearBVH(LinearBVH &&);
  LinearBVH & operator = (LinearBVH &&);

  static void construct_from_host_spheres(LinearBVH& bvh, const Sphere * host_spheres, uint32_t num_spheres);
  static void construct_from_dev_spheres(LinearBVH& bvh, const Sphere * dev_spheres, uint32_t num_spheres);

  // Non-owning reference into an existing tree
  // Can be copied onto the device to actually do ray tracing
  class Ref {
    InnerNode *inner_nodes_ = nullptr;
    LeafNode *leaf_nodes_ = nullptr;
    Sphere *spheres_ = nullptr;
    uint32_t *map_ = nullptr;

    uint32_t num_spheres_ = 0;
    uint32_t num_inner_nodes_ = 0;
    uint32_t num_leaf_nodes_ = 0;

  public:
    Ref(const LinearBVH & tree);

#ifdef __CUDACC__
    template <int STACK_SIZE=32, int THREADS_PER_BLOCK=512,  typename Func>
    __device__ void visitRayIntersects(D_Ray ray, Func visit) const;
#endif
  };

  Ref get_ref() const;

  const uint32_t * get_map() const {
    return map_;
  }
  const Sphere * get_spheres() const {
    return spheres_;
  }

  LinearBVH(uint32_t num_spheres);

private:
  InnerNode *inner_nodes_ = nullptr;
  LeafNode *leaf_nodes_ = nullptr;
  Sphere *spheres_ = nullptr;
  uint32_t *map_ = nullptr;

  uint32_t num_spheres_ = 0;
  uint32_t num_inner_nodes_ = 0;
  uint32_t num_leaf_nodes_ = 0;

  void build_tree(const Sphere * in_spheres);
  void validate_tree() const;
};

#ifdef __CUDACC__
template <int STACK_SIZE, int THREADS_PER_BLOCK,  typename Func>
__device__ void LinearBVH::Ref::visitRayIntersects(const D_Ray ray, Func visit) const {
  constexpr int STACK_SENTINEL = -128;
  constexpr int WARP_SIZE = 32;
  static_assert(THREADS_PER_BLOCK % WARP_SIZE == 0, "Invalid THREADS_PER_BLOCK specified");
  constexpr int WARPS_IN_BLOCK = (THREADS_PER_BLOCK/32);

  auto visit_leaf =
    [&](const LinearBVH::LeafNode& leaf) {
      for (int i = 0; i < leaf.num_spheres; ++i) {
        // enforce constness on the visitor
        auto isphere = leaf.first_sphere + i;
        const auto sph = spheres_[isphere];
        visit(isphere, sph.pos, sph.radius);
      }
    };

  // Trivial case, no inner nodes
  if (num_inner_nodes_ == 0) {
    if (num_spheres_ > 0) {
      visit_leaf(leaf_nodes_[0]);
    } else {
      return;
    }
  }

  // const int lane = threadIdx.x % WARP_SIZE;
  const int wid = threadIdx.x / WARP_SIZE;
  __shared__ int stack[STACK_SIZE * WARPS_IN_BLOCK];
  int* stack_ptr = stack + STACK_SIZE * wid;  // warps use the same stack
  *stack_ptr = STACK_SENTINEL;
  float3 OoD;
  OoD.x = ray.origin.x * ray.invdir.x;
  OoD.y = ray.origin.y * ray.invdir.y;
  OoD.z = ray.origin.z * ray.invdir.z;
  stack_ptr++;
  *stack_ptr = 0;  // push root to stack

  while (*stack_ptr > STACK_SENTINEL) {  // while stack is not empty
    assert(stack_ptr < stack + STACK_SIZE * (wid + 1));
    int currentNode = *stack_ptr;
    --stack_ptr;
    // fetch the BVH node
    const auto node = inner_nodes_[currentNode];

    bool overlap_left = node.left_bb.intersect(ray, OoD);
    bool overlap_right = node.right_bb.intersect(ray, OoD);

    if (__any_sync(__activemask(), overlap_left)) {
      if (node.flags & LinearBVH::left_leaf_flag) {
        const auto leaf = leaf_nodes_[node.left_child];
        visit_leaf(leaf);
      } else {
        ++stack_ptr;
        *stack_ptr = node.left_child;
      }
    }

    if (__any_sync(__activemask(), overlap_right)) {
      if (node.flags & LinearBVH::right_leaf_flag) {
        const auto leaf = leaf_nodes_[node.right_child];
        visit_leaf(leaf);
      } else {
        ++stack_ptr;
        *stack_ptr = node.right_child;
      }
    }
  }
}
#endif // __CUDACC__
