ifndef MAKE_CONFIG
 MAKE_CONFIG = ../make.config
endif
include $(MAKE_CONFIG)
# The following need to be defined:
#  CXX, CXXFLAGS
#  NVCC, NVCCFLAGS
#  AR, AR_FLAGS
#  INC_LIBCUDASUPPORT

# Set defaults, if you want to compile from just the library.
ifndef CXX
 CXX = g++
endif
ifndef CXXFLAGS
 CXXFLAGS = -O2
endif
ifndef NVCC
 NVCC = nvcc
endif
ifndef NVCCFLAGS
 NVCCFLAGS = -Xptxas -O3 -Xcompiler "-fopenmp" -std=c++11
endif
ifndef AR
 AR = gcc-ar
endif

INCS = $(INC_LIBCUDASUPPORT) 

LD   = $(NVCC)

LD_FLAGS =

EXEC = testTree
LIBRARY = librtsph.a

SRC_EXEC = testTree.cu 
OBJS_EXEC = $(SRC_EXEC:.cu=.o)

SRC_CPP = mkSAHBVH.cc  Parser.cc G2Dataset.cc BVH.cc

OBJS_CPP = $(SRC_CPP:.cc=.o)

SRC_CUDA = D_Rays.cu Weight.cu D_BVH.cu IntegrateAlongRay.cu LinearBVH.cu

OBJS_CUDA = $(SRC_CUDA:.cu=.o)

LINK_CUDA = $(SRC_CUDA:.cu=_link.o)

LIB_ARCH = -lgomp
NVCC_LIBS = -L$(CUDA_PATH)/lib64 -lcudart -lcurand -lcudadevrt

ADDITIONAL_NVCCFLAGS := --extended-lambda --expt-relaxed-constexpr

LIBS = $(LIB_ARCH) -lm $(NVCC_LIBS)

all: lib

$(EXEC): $(OBJS_EXEC) $(LIBRARY)
	$(LD) -o $(EXEC) $(LD_FLAGS) $(NVCCFLAGS) $(OBJS_EXEC) $(LIBS) $(LIBRARY)

lib: $(OBJS_CPP) $(OBJS_CUDA)
	$(AR) $(AR_FLAGS) r $(LIBRARY) $(OBJS_CPP) $(OBJS_CUDA) $(LINK_CUDA)

clean:
	-rm *.o
	-rm *~
	-rm Make.log

distclean: clean
	-rm $(EXEC)
	-rm $(LIBRARY)

rebuild: distclean lib

.SUFFIXES: .o .cc .cu

.cc.o:
	$(CXX) $(CXXFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	@echo Making $< into $@
	$(NVCC) -lcudadevrt $(NVCCFLAGS) $(ADDITIONAL_NVCCFLAGS) $(DEFS) $(INCS) -dc $<
	@echo linking to $(*F)_link.o
	$(NVCC) $(NVCCFLAGS) -dlink -o $(*F)_link.o $@


print-% : ; @echo $* = $($*)
