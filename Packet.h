#pragma once

#include "Common.h"
#include "Ray.h"
#include "Rays.h"

#include <iostream>

#if defined __SIMD__
#define PACKET_COUNT 64
#define PACKET_SIZE PACKET_COUNT/__WIDTH__

/// \brief Packet is a collection of similar rays.
///
/// Packets should be constructed from coherent rays, 
/// which have close origins and directions. This
/// allows vertical SIMD optimization with better
/// scaling than what the compiler can automatically
/// do. Incoherent rays will lead to greater per ray
/// calculation and therefore are not good, because
/// each ray will perform all intersection tests.
struct Packet {
    RTSPH_FLOATw ox[PACKET_SIZE];
    RTSPH_FLOATw oy[PACKET_SIZE];
    RTSPH_FLOATw oz[PACKET_SIZE];
    
    RTSPH_FLOATw dx[PACKET_SIZE];
    RTSPH_FLOATw dy[PACKET_SIZE];
    RTSPH_FLOATw dz[PACKET_SIZE];
    
    //RTSPH_FLOATw tmin[PACKET_SIZE];
    RTSPH_FLOATw tmax[PACKET_SIZE];

    RTSPH_FLOATw invdx[PACKET_SIZE];
    RTSPH_FLOATw invdy[PACKET_SIZE];
    RTSPH_FLOATw invdz[PACKET_SIZE];
    
    uint64_t ids[PACKET_COUNT];

    ///
    ///
    ///
    Packet() {}

    ///
    ///
    ///
    Packet(const Rays& rays) {
        //check size
        assert(rays.count() == PACKET_COUNT);

        //copy into sse
        for(size_t i = 0; i < PACKET_SIZE; ++i) {
            ox[i] = loadupd(&rays.ox[i*__WIDTH__]);
            oy[i] = loadupd(&rays.oy[i*__WIDTH__]);
            oz[i] = loadupd(&rays.oz[i*__WIDTH__]);
            
            dx[i] = loadupd(&rays.dx[i*__WIDTH__]);
            dy[i] = loadupd(&rays.dy[i*__WIDTH__]);
            dz[i] = loadupd(&rays.dz[i*__WIDTH__]);
            
            //tmin[i] = loadupd(&rays.tmin[i*__WIDTH__]);
            tmax[i] = loadupd(&rays.tmax[i*__WIDTH__]);

            invdx[i] = divpd(one, dx[i]);
            invdy[i] = divpd(one, dy[i]);
            invdz[i] = divpd(one, dz[i]);
        }

        for(size_t i = 0; i < PACKET_COUNT; ++i) {
            ids[i] = rays.id[i];
        }
    }
};

/// \brief Container for packets.
///
///
struct Packets {
    std::vector<Packet> packets;
};
#endif

