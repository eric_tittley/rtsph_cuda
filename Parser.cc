#include <fstream>
#include <iostream>

#include "Parser.h"

bool parse(const std::string& filename, Spheres& spheres) {
    //open file
    std::ifstream stream;
    stream.open(filename.c_str(), std::ifstream::in|std::ifstream::binary);

    //check file
    if(!stream.good()) {
        std::cerr << "Failed to load: " << filename << std::endl;
        return false;
    }

    //parse header
    uint32_t npart[6];
    RTSPH_FLOAT hmass[6];

    stream.seekg(4,stream.cur);
    stream.read((char*)npart,         6*sizeof(uint32_t));
    stream.read((char*)hmass,         6*sizeof(RTSPH_FLOAT));

    stream.seekg(sizeof(RTSPH_FLOAT)*6, stream.cur);
    stream.seekg(sizeof(uint32_t)*13, stream.cur);

    //stream.read((char*)&spheres.time,           sizeof(RTSPH_FLOAT));
    //stream.read((char*)&spheres.redshift,       sizeof(RTSPH_FLOAT));
    
    //stream.read((char*)&spheres.sfr,            sizeof(uint32_t));
    //stream.read((char*)&spheres.feedback,       sizeof(uint32_t));
    //stream.read((char*)spheres.npartTotal,    6*sizeof(uint32_t));
    //stream.read((char*)&spheres.flag_cooling,   sizeof(uint32_t));
    //stream.read((char*)&spheres.num_files,      sizeof(uint32_t));
    
    //stream.read((char*)&spheres.BoxSize,        sizeof(RTSPH_FLOAT));
    //stream.read((char*)&spheres.Omega0,         sizeof(RTSPH_FLOAT));
    //stream.read((char*)&spheres.OmegaLambda,    sizeof(RTSPH_FLOAT));
    //stream.read((char*)&spheres.HubbleParam,    sizeof(RTSPH_FLOAT));

    //stream.read((char*)&spheres.flag_stellarage,sizeof(uint32_t));
    //stream.read((char*)&spheres.flag_metals,    sizeof(uint32_t));
    //stream.read((char*)&spheres.hashtabsize,    sizeof(uint32_t));

    stream.seekg(84,stream.cur);
    stream.seekg(4,stream.cur);

    //uint32_t nTotal = spheres.npart[0]+spheres.npart[1]+
    //                  spheres.npart[2]+spheres.npart[3]+
    //                  spheres.npart[4]+spheres.npart[5];
    uint32_t nTotal = npart[0] + npart[1] + npart[2] +
                      npart[3] + npart[4] + npart[5];

    //parse positions
    float* pos = new float[npart[0]*3];
   
    stream.seekg(4,stream.cur);
    stream.read((char*)pos, npart[0]*3*sizeof(float));

    stream.seekg(npart[1]*3*sizeof(float), stream.cur);
    stream.seekg(npart[2]*3*sizeof(float), stream.cur);
    stream.seekg(npart[3]*3*sizeof(float), stream.cur);
    stream.seekg(npart[4]*3*sizeof(float), stream.cur);
    stream.seekg(npart[5]*3*sizeof(float), stream.cur);

    stream.seekg(4,stream.cur);

    //Skip V, ID
    stream.seekg(4,stream.cur);
    stream.seekg(nTotal*3*sizeof(float),stream.cur);
    stream.seekg(4,stream.cur);
    stream.seekg(4,stream.cur);
    stream.seekg(nTotal*sizeof(uint32_t),stream.cur);
    stream.seekg(4,stream.cur);

    //Skip mass section
    uint32_t parts = 0;
    for(uint32_t i = 0; i < 6; ++i) {
        if((npart[i] > 0) && (hmass[i] == 0.0)) {
            parts += npart[i];
        }
    }

    float *mass = 0;
    if(parts > 0) {
        stream.seekg(4,stream.cur);
        //stream.seekg(parts*sizeof(float),stream.cur);

        mass = new float[parts];
        stream.read((char*)mass, parts*sizeof(float));
        stream.seekg(4,stream.cur);
    }

    //Gas specific data
    float *radii = 0;
    if(npart[0] > 0) {
        //Read Thermal
        stream.seekg(4,stream.cur);
        //u = new float[spheres.npart[0]];
        //stream.read((char*)u,spheres.npart[0]*sizeof(float));
        stream.seekg(npart[0]*sizeof(float), stream.cur);
        stream.seekg(4,stream.cur);

        //Read Density
        stream.seekg(4,stream.cur);
        //rho = new float[spheres.npart[0]];
        //stream.read((char*)rho,spheres.npart[0]*sizeof(float));
        stream.seekg(npart[0]*sizeof(float), stream.cur);
        stream.seekg(4,stream.cur);

        //Read H radii
        stream.seekg(4,stream.cur);
        radii = new float[npart[0]];
        stream.read((char*)radii,npart[0]*sizeof(float));
        stream.seekg(4,stream.cur);

        stream.close();
    }
    else {
        std::cerr << "File " << filename << " does not contain ";
        std::cerr << "information about gas." << std::endl;
        stream.close();
        return false;
    }
     
    //write out
    for(uint32_t idx = 0; idx < npart[0]; ++idx) {
        spheres.cx.push_back(pos[idx*3+0]);
        spheres.cy.push_back(pos[idx*3+1]);
        spheres.cz.push_back(pos[idx*3+2]);
        //spheres.h.push_back(radii[idx]);

        spheres.invh2.push_back(1.0/(radii[idx]*radii[idx]));

        //if(mass == 0) {
        //    spheres.q.push_back(hmass[0]);
        //}
        //else {
        //    spheres.q.push_back(mass[idx]);
        //}

        //spheres.r.push_back(radii[idx]);
        //spheres.r2.push_back(radii[idx]*radii[idx]);
        //spheres.u.push_back(u[idx]);
        //spheres.rho.push_back(rho[idx]);
    }

    delete radii;

    return true;
}
