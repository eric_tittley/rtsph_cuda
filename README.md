
An implementation of a CUDA based ray tracer that calculates column densities along rays through a field of SPH particles. 

Most of the important functions are located in D_BVH.cu except the kernel interpolation scheme which is in Weight.cu.

To call the kernels something like this should work

int stepSize = RAYS_PER_BLOCK*NUM_BLOCKS;
int numSteps = size/stepSize + 1;
for(int i=0; i<numSteps; i++) {
	printf("starting step %d of %d\n", i, numSteps);
	launchFindIntersectsKernel(d_rays, d_tree, d_weights, d_q, i);
}


copying the BVH tree and rays can be done with (there are also functions to free the memory called freeTree() and freeRays())

D_BVH * d_tree = copyTreeToDevice(&tree);
D_Rays * d_rays = copyRaysToDevice(&rays);

The weights and q values need to be copied manually at the moment. 

All cuda related parameters (block/grid size) can be found in cuda_constants.h
