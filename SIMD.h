#pragma once

//Library Includes
#include <x86intrin.h>
#include <iostream>

//Common ILP definitions
#if defined __SSE4_2__ || __AVX__
#define __SIMD__ 1
typedef __m128i int4;
#define storesi128 _mm_store_si128
#endif

//AVX specific defintions
#if defined __AVX__
#define __WIDTH__ 4
typedef __m256d double4;
typedef double4 doublew;

//AVX constants
const doublew zero = _mm256_setzero_pd();
const doublew one  = _mm256_set1_pd(1.0);
//const doublew half = _mm256_set1_pd(0.5);
//const doublew negone = _mm256_set1_pd(-1.0);
//const doublew truew = _mm256_cmp_pd(zero, zero, _CMP_EQ_OS);
const doublew falsew = _mm256_cmp_pd(zero, one, _CMP_EQ_OS);

//const doublew seq  = _mm256_set_pd(1.0, 2.0, 3.0, 4.0);

//AVX aliases
#define loadpd _mm256_load_pd
#define loadupd _mm256_loadu_pd
#define set1pd _mm256_set1_pd
#define setpd _mm256_set_pd
#define storepd _mm256_store_pd
#define storeupd _mm256_storeu_pd

#define cvttpd_epi32 _mm256_cvttpd_epi32
#define cvtepi32_pd _mm256_cvtepi32_pd

#define addpd _mm256_add_pd
#define subpd _mm256_sub_pd
#define mulpd _mm256_mul_pd
#define divpd _mm256_div_pd
#define sqrtpd _mm256_sqrt_pd
#define minpd _mm256_min_pd
#define maxpd _mm256_max_pd

#define andpd _mm256_and_pd
#define andnotpd _mm256_andnot_pd
#define orpd _mm256_or_pd
#define movemaskpd _mm256_movemask_pd

#define cmpltpd(A,B) _mm256_cmp_pd(A,B,_CMP_LT_OS)
#define cmplepd(A,B) _mm256_cmp_pd(A,B,_CMP_LE_OS)
#define cmpeqpd(A,B) _mm256_cmp_pd(A,B,_CMP_EQ_OS)
#define cmpgtpd(A,B) _mm256_cmp_pd(A,B,_CMP_GT_OS)
#define cmpgepd(A,B) _mm256_cmp_pd(A,B,_CMP_GE_OS)

inline void print(doublew wide) {
    double tmp[4];
    storepd(tmp, wide);
    std::cout << tmp[0] << " " << tmp[1] << " " << tmp[2];
    std::cout << " " << tmp[3] << std::endl;
}

inline void print(doublew wide[4]) {
    for(size_t i = 0; i < 4; ++i) {
        double tmp[4];
        storepd(tmp, wide[i]);
        std::cout << tmp[0] << " " << tmp[1] << " " << tmp[2];
        std::cout << " " << tmp[3] << " - ";
    }
    std::cout << std::endl;
}

//SSE 4.2 specific definitions
#elif defined __SSE4_2__
#define __WIDTH__ 2
typedef __m128d double2;
typedef double2 doublew;

//SSE4.2 constants
const doublew zero = _mm_setzero_pd();
const doublew one  = _mm_set1_pd(1.0);
const doublew half = _mm_set1_pd(0.5);
const doublew negone = _mm_set1_pd(-1.0);
const doublew truew = _mm_cmpeq_pd(zero, zero);
const doublew falsew = _mm_cmpeq_pd(zero, one);

//SSE4.2 aliases
#define loadpd _mm_load_pd
#define loadupd _mm_loadu_pd
#define set1pd _mm_set1_pd
#define setpd _mm_set_pd
#define storepd _mm_store_pd
#define storeupd _mm_storeu_pd

#define cvttpd_epi32 _mm_cvttpd_epi32
#define cvtepi32_pd _mm_cvtepi32_pd

#define addpd _mm_add_pd
#define subpd _mm_sub_pd
#define mulpd _mm_mul_pd
#define divpd _mm_div_pd
#define sqrtpd _mm_sqrt_pd
#define minpd _mm_min_pd
#define maxpd _mm_max_pd

#define andpd _mm_and_pd
#define andnotpd _mm_andnot_pd
#define orpd _mm_or_pd
#define movemaskpd _mm_movemask_pd

#define cmpltpd _mm_cmplt_pd
#define cmplepd _mm_cmple_pd
#define cmpeqpd _mm_cmpeq_pd
#define cmpgtpd _mm_cmpgt_pd
#define cmpgepd _mm_cmpge_pd

inline void print(doublew wide) {
    RTSPH_FLOAT tmp[2];
    storepd(tmp, wide);
    std::cout << tmp[0] << " " << tmp[1] << std::endl;
}

inline void print(doublew wide[8]) {
    for(size_t i = 0; i < 8; ++i) {
        RTSPH_FLOAT tmp[2];
        storepd(tmp, wide[i]);
        std::cout << tmp[0] << " " << tmp[1] << " ";
    }
    std::cout << std::endl;
}

#endif
