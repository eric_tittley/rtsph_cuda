#pragma once
/// \file Spheres.h
/// \brief Sphere(SOA) structure

#include <vector>

#include "Common.h"
#include "Ray.h"

/// \brief Contains all geometry data required for ray tracing and integration.
///
///
struct Spheres {
    Spheres() = default;
    Spheres(size_t count) {
        resize(count);
    }

    /// \brief Parallel array for x coordinate of spheres.
    std::vector<float> cx;

    /// \brief Parallel array for y coordinate of spheres.
    std::vector<float> cy;

    /// \brief Parallel array for z coordinate of spheres.
    std::vector<float> cz;

    /// \brief Parallel array for inverse radius of spheres.
    std::vector<float> invh2;
    
    std::vector<float> h; 
    /// \brief Number of spheres.
    size_t count() const {
        return cx.size();
    }

    /// \brief Resize all sphere component arrays
    void resize(size_t count) {
        cx.resize(count);
        cy.resize(count);
        cz.resize(count);
        invh2.resize(count);
        h.resize(count);
    }

    /// \brief Reserve memory in all sphere component arrays
    void reserve(size_t count) {
        cx.reserve(count);
        cy.reserve(count);
        cz.reserve(count);
        invh2.reserve(count);
        h.reserve(count);
    }

    /// \brief Fetch Vector3 center for indexed sphere.
    void getCenter(size_t idx, Vector3& center) const {
        center.x() = (RTSPH_FLOAT)cx[idx];
        center.y() = (RTSPH_FLOAT)cy[idx];
        center.z() = (RTSPH_FLOAT)cz[idx];
    }

    /// \brief Intersect the indexed sphere with ray.
    ///
    ///
    //bool intersect(size_t, const Ray&, RayIntersect&) const;
    inline RTSPH_FLOAT intersect(size_t, const Ray&) const;

    /*#if defined __SIMD__
    ///
    ///
    ///
    void intersect(size_t, const Packet&, PacketIntersect&) const;
    #endif*/
};

RTSPH_FLOAT Spheres::intersect(size_t isph, const Ray& ray) const {
    //local space
    Vector3 local; getCenter(isph, local); 
    local -= ray.origin;
    RTSPH_FLOAT l2 = local.dot(local);

    //project onto direction
    RTSPH_FLOAT tb = ray.direction.dot(local);

    //pythagoras
    RTSPH_FLOAT tb2 = tb*tb;
    RTSPH_FLOAT b2 = l2 - tb2;

    //square normalized impact parameter
    RTSPH_FLOAT nb2 = b2 * invh2[isph];

    //impose roots & ray limits & filtering
    //1.0 will contribute 0.0 as kernel value
    return (nb2<=1.0) && (0.0<tb) && (tb<ray.tmax) ? nb2 : 1.0;
}
