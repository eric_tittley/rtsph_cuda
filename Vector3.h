#pragma once

#include <cmath>
#include <cstdlib>

#include "Common.h"

struct Vector3 {
private:
    RTSPH_FLOAT _x, _y, _z;

public:
    inline RTSPH_FLOAT x() const {
        return _x;
    }
    
    inline RTSPH_FLOAT y() const {
        return _y;
    }
    
    inline RTSPH_FLOAT z() const {
        return _z;
    }

    inline RTSPH_FLOAT& x() {
        return _x;
    }
    
    inline RTSPH_FLOAT& y() {
        return _y;
    }
    
    inline RTSPH_FLOAT& z() {
        return _z;
    }

    inline RTSPH_FLOAT operator[](size_t idx) const {
        return idx==0?x():(idx==1?y():z());
    }

    inline Vector3() 
    : _x(0.0), _y(0.0), _z(0.0) {}

    inline Vector3(RTSPH_FLOAT cx, RTSPH_FLOAT cy, RTSPH_FLOAT cz) 
    : _x(cx), _y(cy), _z(cz) {}
    
    inline Vector3(RTSPH_FLOAT c) 
    : _x(c), _y(c), _z(c) {}
    
    inline Vector3(const Vector3& v) 
    : _x(v.x()), _y(v.y()), _z(v.z()) {}

    uint32_t longestAxis() {
        if((x() > y()) && (x() > z())) return 0;
        else return (y() > z()) ? 1 : 2;
    }
    
    inline void operator+=(const Vector3& v) {
        x() += v.x(); y() += v.y(); z() += v.z();
    }

    inline void operator-=(const Vector3& v) {
        x() -= v.x(); y() -= v.y(); z() -= v.z();
    }

    inline void operator*=(RTSPH_FLOAT c) {
        x() *= c; y() *= c; z() *= c;
    }

    inline Vector3 operator*(RTSPH_FLOAT c) const {
        return Vector3(x()*c, y()*c, z()*c);
    }

    inline Vector3 operator-(const Vector3& v) const {
        return Vector3(x() - v.x(), y() - v.y(), z() - v.z());
    }

    inline void operator/=(RTSPH_FLOAT c) {
        x() /= c; y() /= c; z() /= c;
    }
    
    inline RTSPH_FLOAT dot(const Vector3& v) const {
        return (x() * v.x()) 
             + (y() * v.y()) 
             + (z() * v.z());
    }

    inline Vector3 cross(const Vector3& v) const {
        return Vector3(y()*v.z() - z()*v.y(),
                       z()*v.x() - x()*v.z(),
                       x()*v.y() - y()*v.x());
    }

    inline RTSPH_FLOAT length2() const {
        return (*this).dot(*this);
    }

    inline RTSPH_FLOAT length() const {
        return sqrt(length2());
    }

    inline void comMul(const Vector3& v) {
        x() *= v.x();
        y() *= v.y();
        z() *= v.z();
    }

    void normalize() {
        RTSPH_FLOAT l = length();
        //XXX could avoid by addition epsilon
        if(l > 0.0) (*this) /= l; //else ignore
    }

    Vector3 normalized() const {
        Vector3 t(*this); 
        t.normalize();
        return t;
    }

    inline RTSPH_FLOAT minelm() const {
        return MIN(x(), MIN(y(), z()));
    }

    inline RTSPH_FLOAT maxelm() const {
        return MAX(x(), MAX(y(), z()));
    }

    inline Vector3 minpel(const Vector3& v) const {
        return Vector3(MIN(x(), v.x()),
                       MIN(y(), v.y()), 
                       MIN(z(), v.z()) );
    }

    inline Vector3 maxpel(const Vector3& v) const {
        return Vector3(MAX(x(), v.x()),
                       MAX(y(), v.y()), 
                       MAX(z(), v.z()) );
    }

    inline void min(const Vector3& v) {
        x() = MIN(x(), v.x()); 
        y() = MIN(y(), v.y()); 
        z() = MIN(z(), v.z());
    }

    inline void max(const Vector3& v) {
        x() = MAX(x(), v.x()); 
        y() = MAX(y(), v.y()); 
        z() = MAX(z(), v.z());
    }

    inline void set(RTSPH_FLOAT c) {
        x() = c;
        y() = c;
        z() = c;
    }

    inline void set(RTSPH_FLOAT cx, RTSPH_FLOAT cy, RTSPH_FLOAT cz) {
        x() = cx;
        y() = cy;
        z() = cz;
    }

    void set(const Vector3& v) {
        x() = v.x();
        y() = v.y();
        z() = v.z();
    }

    Vector3 inverse() const {
        return Vector3( 1.0/x(),
                        1.0/y(),
                        1.0/z());
    }
};

inline RTSPH_FLOAT randreal() {
    return (RTSPH_FLOAT)rand()/(RTSPH_FLOAT)RAND_MAX;
}

inline Vector3 randVector3() {
    return Vector3(randreal(), randreal(), randreal());
}
