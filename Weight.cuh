#pragma once
// \file Weight.h
/// \brief Includes all look up table related functions and data.
///
/// This file contains the prepared look up table of the integral
/// of the Gadget2 SPH Kernel and functions to calculate the weight
/// contributed by a sphere which is intersected by a ray.
constexpr int ARRSIZE = 501;

#include "Common.h"
#include <cudasupport.h>

struct Weights {
  cudaTextureObject_t texture_;
  cudaArray_t array_;

public:

  Weights();
  ~Weights();

  class DevInterface {
    cudaTextureObject_t texture_;
  public:
    DevInterface(const Weights & weights) : texture_(weights.texture_) {}

    ERT_DEVICE float getRayImpactWeight(float nb2) const;
  };

  DevInterface device() const {
    return DevInterface(*this);
  }
};

#ifdef __CUDACC__
inline __device__ float Weights::DevInterface::getRayImpactWeight(float nb2) const {
  constexpr float OFFSET = static_cast<float>(1.0/(2.0*ARRSIZE));
  return tex1D<float>(texture_, nb2+OFFSET);
}
#endif
