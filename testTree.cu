#include <algorithm>
#include <random>
#include <omp.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <chrono>


#include "cuda_profiler_api.h"

#include "BVH.h"
#include "D_BVH.cuh"
#include "G2Dataset.h"
#include "Rays.h"
#include "Spheres.h"
#include "Vector3.h"


#define OMP_CHUNK_SIZE 2048

#define SIZE 1024
//#define RANDOM
//#define UNIFORM
#define RAYS_TO_PARTICLES

__host__ float* runKernels(D_Rays* d_rays, D_BVH* d_tree, int size);
         RTSPH_FLOAT* runOMP(Rays* rays, BVH* tree, size_t size);

void makeTree(const G2Dataset& data, BVH& tree) {
    //extract spheres from G2 data
    Spheres sphs; data.getSpheres(sphs);

    //build tree
    mkSAHBVH(sphs, tree);
}

int main(int argc, char** argv) {

    RTSPH_FLOAT start, end;
    
    printf("parsing\n");
    G2Dataset data;
    std::string filename("Data_025");
    start = omp_get_wtime();
    parseG2Dataset(filename, data);
    end = omp_get_wtime();
    printf("parsed \n");

    //generate tree
    printf("building\n");
    BVH tree;
    makeTree(data, tree);
    end = omp_get_wtime();
    printf("built \n");
    printf("%lu nodes\n", tree.nodes.size());
    //computation
    printf("%lu spheres\n", tree.spheres.cx.size());
 
    //generate rays
    printf("generate rays\n");
    Rays rays;
    int numSpheres = tree.spheres.cx.size();
    
#ifdef UNIFORM
    for(int i=0; i<SIZE; i+=8) {
       for(int j=0; j<SIZE; j+=8) {
    	    for(int ii=0; ii<8; ii++) {
    		    for(int jj=0; jj<8; jj++) {
    		    Vector3 p1, p2;
    		    RTSPH_FLOAT x = ((RTSPH_FLOAT)(i+ii)/SIZE)*10000.0;
    		    RTSPH_FLOAT y = ((RTSPH_FLOAT)(j+jj)/SIZE)*10000.0;
    		    p1.x() = x; p1.y() = y; p1.z() = 0;
    		    p2.x() = x; p2.y() = y; p2.z() = 10000.0;	    
    		    rays.setFromPoints(p1, p2, i);
    		    }
    	    }
       }
    }
#endif
#ifdef RANDOM
  std::mt19937_64 rng;
  // initialize the random number generator with time-dependent seed
  uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed>>32)};
  rng.seed(ss);
  // initialize a uniform distribution between 0 and 1
  std::uniform_real_distribution<double> unif(0, 1);
  // ready to generate random numbers
  for(int i=0; i<SIZE*SIZE; i++) {
    Vector3 p1, p2;
    RTSPH_FLOAT x = unif(rng)*10000.0;
    RTSPH_FLOAT y = unif(rng)*10000.0;
    p1.x() = x; p1.y() = y; p1.z() = 0;
    p2.x() = x; p2.y() = y; p2.z() = 10000.0;	    
    rays.setFromPoints(p1, p2, i);
  }
#endif
#ifdef RAYS_TO_PARTICLES
  Vector3 RayStart(0.0, 0.0, 0.0); 
  // Create rays from the source to all particles
  int iParticle;
  for(iParticle=0;iParticle<numSpheres;++iParticle) {
    Vector3 RayFinish(tree.spheres.cx[iParticle],
                      tree.spheres.cy[iParticle],
                      tree.spheres.cz[iParticle]);
    rays.setFromPoints(RayStart, RayFinish, iParticle);
  }

#endif
    size_t size = rays.ox.size();
    FILE * FP;
    start = omp_get_wtime();
    printf("processing rays \n");
    D_BVH * d_tree = copyTreeToDevice(&tree);
    D_Rays * d_rays = copyRaysToDevice(&rays);
    cudaDeviceSynchronize();
    float *fweights;
    printf("Running kernel...\n");
    fweights = runKernels(d_rays, d_tree, (int)size);
    end = omp_get_wtime();
//    freeRays(d_rays);
//    freeTree(d_tree);
    printf("processing done  %f seconds \n", (end-start));

    /* Write the GPU weights */
    FP=fopen("weightsGPU.dat","w");
    fwrite(fweights,sizeof(float),size,FP);
    fclose(FP);
}


__host__ float* runKernels(D_Rays* d_rays, D_BVH* d_tree, int size) {


	//copies textures for interpolation
	cpweights();
	float *weights, *d_weights;

	int numSpheres = 128*128*128;
	int numq = NUMBER_OF_QS;
	weights = (float*) malloc(numq*size*sizeof(float));
	for(int i=0; i<size*numq; i++) {
		weights[i] = 0;
	}
	float *q, *d_q;
	q = (float*) malloc(numSpheres*sizeof(float)*numq);
	for(int i=0; i<numSpheres; i++) {
		q[i] = 1.0;
	//	q[i+numSpheres] = 0.1;
	//	q[i+2*numSpheres] = 0.01;
	}
		
    //copy q values    
	cudaMalloc((void**)&d_q, numq*numSpheres*sizeof(float));
	cudaMemcpy(d_q, &q[0], numq*numSpheres*sizeof(float), cudaMemcpyHostToDevice);
	//copy ray weights to be filled by kernel
	cudaMalloc((void**)&d_weights, numq*size*sizeof(float));
	cudaMemcpy(d_weights, weights, numq*size*sizeof(float), cudaMemcpyHostToDevice);

	//general code to launch kernels for a set number of rays
	//size = number of rays
	launchFindIntersectsKernel(d_rays, d_tree, d_weights, d_q, (size_t)size);

	cudaDeviceSynchronize();
 

	float *filledweights;
	//copy weights back
	filledweights = (float*) malloc(numq*size*sizeof(float));
	cudaMemcpy(filledweights, d_weights, numq*size*sizeof(float), cudaMemcpyDeviceToHost);


	float out = 0.0;
	for(int i=0; i<size*numq; i++) {
		out += filledweights[i];
	}
	for(int i=0; i<10; i++) {
	 printf("%5.3e\n",filledweights[i]);
	}
	float dL = 10000.0/1000;
	float dA = dL*dL;
	out*=dA;
	printf("Total is: %f\n", out);
	printf("done\n");

	return filledweights;
}


RTSPH_FLOAT* runOMP(Rays* rays, BVH* tree, size_t size) {
 RTSPH_FLOAT * weights;
 size_t const numSpheres = 128*128*128;
 weights = (RTSPH_FLOAT*) calloc(size,sizeof(RTSPH_FLOAT));

 /* Set the value to integrate */
 RTSPH_FLOAT * q;
 q = (RTSPH_FLOAT*) malloc(numSpheres*sizeof(RTSPH_FLOAT));
 for(size_t i=0; i<numSpheres; i++) {
  q[i] = 1.0;
 }

#pragma omp parallel for default(none) \
        shared(rays,tree,q,weights) \
        schedule(dynamic,OMP_CHUNK_SIZE)
 for(size_t iray = 0; iray < rays->count(); ++iray) {
  /* Pick out a single ray */
  Ray ray;
  rays->get(iray,ray);
  /* Perform the integration along the ray */
  weights[iray] = tree->weightRay(ray,q);
 }

 /* Sum up the weights */
 RTSPH_FLOAT out = 0.0;
 for(size_t i=0; i<size; i++) {
 	 out += weights[i];
 }
 for(int i=0; i<10; i++) {
  printf("%5.3e\n",weights[i]);
 }
 RTSPH_FLOAT dL = 10000.0/1000;
 RTSPH_FLOAT dA = dL*dL;
 out*=dA;
 printf("Total is: %f\n", out);
 
 return weights;

}
